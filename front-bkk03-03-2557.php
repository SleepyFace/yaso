<?phpheader("Content-Type:text/html; charset=utf-8");
	@session_start();
	if(!isset($_SESSION["uname"]) || !isset($_SESSION["front_pivcode"])){
		echo "<br/><br/><center>กรุณา login ใหม่!!.</center>";
		exit(0);
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script type="text/javascript" src="ajax_framework.js"></script>
<script type="text/javascript">
<!--

function preview(){
	
	document.getElementById("divBillPreview").innerHTML = "";
	var data = "data=" + document.getElementById("txtNumber").value;
	data+="&match_date=" + document.getElementById("lbMatchDate").innerHTML;
	data+="&money=" +  document.getElementById("txtMoney").value;
	data+="&pivcode=" + document.getElementById("txtpivCode").value;
	data+="&tbtype="+ document.getElementById("txttbType").value;
	ajaxLoad('post','bill_prvBeforeInsert.php',data,'divBillPreview');
	document.getElementById("divMaskBillPreview").style.left = ((screen.width / 2) - 550) + "px";
	document.getElementById("divMaskBillPreview").style.display = "block";

	document.getElementById("divBillPreview").style.left = ((screen.width / 2) - 350) + "px";
	document.getElementById("divBillPreview").style.display = 'block';	
	
	document.getElementById("btSave").focus();
	return false;
}

function bill_insert(){
	document.getElementById("divBillPreview").innerHTML = "<br/><br/><center><font color='#FFFFFF' size='3'>กำลังบันทึก...</font><br/><img src='image/loading.gif' /></center><br/><br/>";
	var data = "data=" + document.getElementById("txtNumber").value;
	data+="&money=" +  document.getElementById("txtMoney").value;
	data+="&match_date=" + document.getElementById("lbMatchDate").innerHTML;
	data+="&pivcode=" + document.getElementById("txtpivCode").value;
	data+="&tbtype="+ document.getElementById("txttbType").value;
	ajaxLoad('post','bill_insert.php',data,'');
}

function none_save(){
	clear_src();
	document.getElementById("txtNumber").value = "";
	document.getElementById("txtMoney").value = "";
	document.getElementById("txtNumber").focus();
}

function clear_src(){
	document.getElementById("divBillPreview").innerHTML = "";
	document.getElementById("divBillPreview").style.display = "none";
	document.getElementById("divMaskBillPreview").style.display = "none";
	
	document.getElementById("txtNumber").focus();
}

function re_key(){
	document.getElementById("divBillPreview").innerHTML = "";
	document.getElementById("divBillPreview").style.display = 'none';
	document.getElementById("divMaskBillPreview").style.display = 'none';
	
	document.getElementById("txtNumber").focus();
}
		
function  stepFormat(){
	document.getElementById("txtNumber").value += ",";
	document.getElementById("txtNumber").focus();
}

function getdata(){
	if(document.getElementById("txtpivCode").value == "" || document.getElementById("txttbType").value == ""){
		alert("เกิดข้อผิดพลาด :: textbox pivcode และ tb type มีค่าว่าง !!. กรุณา Login ใหม่.");
		return false;
	}
		
	if(document.getElementById("txtNumber").value == ""){
		alert("กรุณาระบุหมายเลข !!.");
		document.getElementById("txtNumber").focus();
		return false;
	}else if(document.getElementById("txtMoney").value == "" || document.getElementById("txtMoney").value <  50){
		alert("กรุณาระบุจำนวนเงิน อย่างน้อย 50 บาท !!.");
		document.getElementById("txtMoney").focus();
		return false;
	}
	
	
	var syntax = true;
	var data  = new String(document.getElementById("txtNumber").value);
	var dataArray = data.split(',');
	
	for(var i=0; i < dataArray.length; i++){
		if(dataArray[i] == ""){
			alert("รูปแบบการใส่หมายเลขแทง ไม่ถูกต้อง !!. \n ข้อสังเกต :: \n 1.หน้าและหลังเครื่องหมาย \",\" ต้องมีตัวเลขเสมอ \n 2.เครื่องหมาย \",\" ต้องไม่ซ้อนกัน \n 3.ถ้าแทงบอลเต็งไม่ต้องใส่ \",\" ");
			syntax = false;
			document.getElementById("txtNumber").focus();
			return false;
		}
	
	}//end for()		
	
	if(dataArray.length == 1){ // check บอลเต็ง
		var money_one = parseInt(document.getElementById("lbMAXone").innerHTML);
		if(document.getElementById("txtMoney").value > money_one){
				alert("บอลเต็ง วันนี้ แทงได้ไม่เกิน #  " + money_one + " บาท !!.");
				syntax = false;
				document.getElementById("txtMoney").focus();
				return false;
		}
		
	}else if(dataArray.length > 1){ // check บอลสเต็ป
	    var chk_min_step = parseInt(document.getElementById("lbMINstep").innerHTML);
		var money_step = parseInt(document.getElementById("lbMAXstep").innerHTML);

		if(dataArray.length < chk_min_step){
				alert("บอลสเต็ป วันนี้ ต้องแทงอย่างน้อย  " + chk_min_step + " คู่ ขึ้นไป !!." );
				syntax = false;
				document.getElementById("txtNumber").focus();
				return false;
							
		}else if(document.getElementById("txtMoney").value > money_step){
				alert("บอลสเต็ป วันนี้ แทงได้ไม่เกิน #  " + money_step + " บาท !!.");
				syntax = false;
				document.getElementById("txtMoney").focus();
				return false;
		}
		
		//-----------------------------------------------------------------//
		//Lock การแทงที่ 10 คู่ ต่อบิล
		if(dataArray.length > 10){
				alert("บอลสเต็ปแทงได้ไม่เกิน 10 คู่ เท่านั้น !!. ");
				syntax = false;
				document.getElementById("txtNumber").focus();
				return false;
		}
		//-----------------------------------------------------------------//

   }
	
	
	if(syntax == true){
		document.getElementById("txtMoney").blur();
		preview();		
		return false;
	}
	
}

//Counter page Refresh
var timer;
var n_counter;
function monitoring(){
	document.getElementById("divdisplayTable").innerHTML = "<br/><center><b>Loading..</b><br/><img src='image/loading.gif' /></center><br/><br/>";
	var data = "match_date=" + document.getElementById("lbMatchDate").innerHTML;
	data += "&pivcode=" + document.getElementById("txtpivCode").value;
	data += "&tbtype=" + document.getElementById("txttbType").value;
	
	var data2 = "bnc_id=" + document.getElementById("txt_session_bnc_id").value;
	ajaxLoad('post','chk_setting.php',data2,'');
	
	ajaxLoad('post','front_table.php',data,'divdisplayTable');

	lbMinute.innerHTML= 2;
	lbCount.innerHTML = 60;
	clearTimeout(timer);
	shwTime();
}

function shwTime(){
	
	if(lbCount.innerHTML == "0"){
		lbCount.innerHTML = 60;
		if(lbMinute.innerHTML == "0"){
			monitoring();
			return(0);
		}
		
		lbMinute.innerHTML = parseInt(lbMinute.innerHTML) - 1;
	}
	
	lbCount.innerHTML = parseInt(lbCount.innerHTML) - 1;
	timer = setTimeout("shwTime()",999);

}

function chk_num(f_txt){
	ek=event.keyCode;
	if(ek <48 ||ek >57){
		event.returnValue=false;
	}
	if(ek == 13){
		if(f_txt == "set") stepFormat();
		else if(f_txt == "get") getdata();
	}
}

-->
</script>
<style type="text/css">
<!--
body{
	font-family:Arial, Helvetica, Sans-Serif;
	font-size:13px;
	margin:0px;
	background-color:#AABFFF;
	
	}
.menu_right_border{
	border-right:solid 2px #FFF;
}

input{
	font-size:20px; 
	border:solid 1px #FF7F00;
	}

button{
	cursor:pointer;
}

#td_{
	color:#FFF; 
	font-weight:bold; 
	background-image:url(image/title_bg.png); 
	background-repeat:repeat-x;
}

/*
@media print{
	.el_hidden{display:none;	}
}
*/
-->
</style>

</head>

<body>

<?php
	if(!isset($_SESSION["uname"]) || !isset($_SESSION["front_pivcode"]) || !isset($_SESSION["tbType"])){
		echo "<br/><br/><center>กรุณา login ใหม่!!.</center>";
		exit(0);
	}

require_once("condb.php");
$query = mysql_query("select match_date from match_h where matchStatus='Active' ;",$conn);
$sql_result = mysql_fetch_array($query);
if($sql_result["match_date"] == ""){
	echo "<br/><br/><br/><center><h2><font color='#D40000'>ขออภัย ขณะนี้ยังไม่มีตาราง Active</h2></center>";
	mysql_close($conn);
	exit(0);
}

$match_date = $sql_result["match_date"];

$tb_type = "ราคาส่วนกลาง";
if($_SESSION["tbType"] == "Y") $tb_type = "ราคาปรับเอง";

$arr_date = explode('-',$match_date);
$new_date = $arr_date[2].'-'.$arr_date[1].'-'.$arr_date['0'];

?>

<!-- hidden textbox  สำหรับใว้ check pivcode และ ระบุที่มาของตารางราคา  -->
<input type="hidden" id="txtpivCode" value="<?=$_SESSION["front_pivcode"]?>" />
<input type="hidden" id="txttbType" value="<?=$_SESSION["tbType"]?>" />
<input type="hidden" id="txt_session_bnc_id" value="<?=$_SESSION["bnc_id"]?>" />
<!-- --------------------------------------------------------------------------------------------------- -->


<br/>
<!-- //////////////-------------------------------------------//////////////////////////////-->
<div style="width:1150px; height:auto; background-color:#FFF; margin:auto; border:solid 4px #FFFFFF;">

<!-- #################-------------------->
<div class="el_hidden">
    <div>  
        <table width="100%" cellpadding="1" cellspacing="1" border="0">
        <tr height="40">
        <td width="118" bgcolor="#AADFFF" align="center">
        <button type="button" onclick="window.parent.open_page('front.php');" style="cursor:pointer; width:110px;  height:35px;"><img src="image/refresh.png" /><b>เรียกซ้ำ</b>&nbsp;</button>&nbsp;
        </td>
        
        <td align="center">
        <!-- ทำสี font สีขาว เพื่อ ซ่อนใว้ -->
        <font color="#FFFFFF"><label id="lbMatchDate"><?=$match_date?></label></font>
        <!-- ทำสี font สีขาว เพื่อ ซ่อนใว้ -->
        
        <font color="#000080" size="3">ตารางแข่งวันที่  &nbsp;<?=$new_date?></font></td>
        
        <td align="right" width="300" style="border:solid 1px #C1C1C1;"> กำลังเรียกซ้ำ ในอีก  <img src="image/gif.gif" /> :: &nbsp;
        <font color="#D40000" size="+2"><b>
        <label style="width:100px; border:solid 2px #EBEBEB; background-color:#2A3F00; color:#FFF;">&nbsp;
            <label id="lbMinute">0</label>&nbsp;:
            <label id="lbCount">0</label>&nbsp;
        </label></b></font>&nbsp;นาที. </td>
        </tr>
        
        <tr>
            <td colspan="2"></td>
            <td align="right" style="border:solid 1px #C1C1C1;" bgcolor="#F6F6F6">คุณกำลังใช้ตารางราคาจาก :: <font color="#D40000" size="3"><?=$tb_type?></font>&nbsp;&nbsp;</td>
        </tr>
        </table>
    </div>


    <font color="#553F55"><u>แจ้งเตือน</u>&nbsp;&nbsp; #บอลเต็ง รับไม่เกิน : <b><font color="#D40000"><label id="lbMAXone"></label></font></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#บอลสเต็ปต้องแทง &nbsp;<b><font color="#D40000"><label id="lbMINstep"></label></font></b> คู่ ขึ้นไป &nbsp;รับไม่เกิน : <b><font color="#D40000"><label id="lbMAXstep"></label></font></b></font>
    <br/><br/>
    
    <div style="width:98%; height:43px; margin:auto; background-color:#D4FFFF; border:dotted 1px #A0A0A4;  font-weight:bold; font-size:14px; text-align:center; padding-top:3px;">
    &nbsp;&nbsp;หมายเลขแทง <input type="text" id="txtNumber" onkeypress="return chk_num('set');" size="50"/ >&nbsp;&nbsp;&nbsp;จำนวนเงิน <input type="text" id="txtMoney"  size="10" onkeypress="chk_num('get');" />&nbsp;<button type="button" style="font-size:18px; cursor:pointer; width:100px;  height:35px;"  onclick="getdata();"><img src="image/download.png" /> แทง</button>
    </div>
    
<br/>
</div>
<!-- #################-------------------->

<div id="divdisplayTable"></div>

</div>
<!-- //////////////-------------------------------------------//////////////////////////////-->

<div id="divMaskBillPreview" style="width:1000px; height:1000px; background-color:#000; top:0px; position:absolute; display:none;  filter:Alpha(Opacity=80); opacity:0.8; z-index:9998;">
</div>

<div id="divBillPreview" style="width:600px; height:auto; background-color:#03C; border:solid 2px #FFF; display:none; position:absolute; top:40px; z-index:9999; "></div>

<br/>
</body>
</html>

<script type="text/javascript">
<!--
	monitoring();
-->
</script>


