<?php header("Content-Type:text/html; charset=utf-8");
	@session_start();
		if(!isset($_SESSION["uname"])){	
			echo "<br/><br/><center>กรุณา login ใหม่!!.</center>";
			exit(0);
	}
	
	$userlogin = $_SESSION["uname"];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
<?php
require_once("condb.php");
$bill_id =  $_POST["bill_id"];
$sql = "select bill_h.*,branch.bnc_name, branch.ctm_pvid  
from bill_h inner join branch on bill_h.bnc_id = branch.bnc_id 
where bill_h.bill_id = '".$bill_id."' ; ";

$query_bill_h = mysql_query($sql,$conn);

if(mysql_num_rows($query_bill_h) <= 0){
	echo "<br/><br/><center>ไม่พบเลขบิลที่ระบุ !!.</center><br/><br/>";
	exit(0);
}


///---------else OK--------------------------------//////////////////////////////------------------------///////////////////--------------------------------------------------------------------

$result = mysql_fetch_array($query_bill_h);

if($result["cancel_flag"] == 'Y'){
	echo "<br/><br/><center>เลขบิลนี้ถูกยกเลิกไปแล้ว !!.</center><br/><br/>";
	exit(0);
	
}else if($result["payment_flag"] == 'Y'){
	echo "<br/><br/><center>เลขบิลนี้ถูกจ่ายไปแล้ว !!.</center><br/><br/>";
	exit(0);
	
}

///---------else OK--------------------------------//////////////////////////////------------------------///////////////////--------------------------------------------------------------------

$cost = $result["costs"];
?>

<div style="text-align:right;">วันที่ <?=$result["bill_date"]." ".substr($result["bill_time"],0,5)?>&nbsp;</div>
<center>
<button style="cursor:pointer; width:160px; height:38px;" onclick="payment_save('<?=$bill_id?>','<?=$userlogin?>')">
  <img src="image/save16.png" /><font color="#D40000" size="3"><b>กดบันทึกจ่าย</b></font>
</button>
</center>

<center>
<h3><?=$result["bnc_name"]?></h3>
<font size="+1">บิล(<?=$bill_id?>)&nbsp;&nbsp;&nbsp;เงินแทง(<?=number_format($cost)?>)</font>


<table width="95%" cellpadding="1" cellspacing="1" border="0" bgcolor="#000000">
<tr height="25" align="center" bgcolor="#E9E9E9">
	<th>ผลบอล</th>
    <th>ผลแทง</th>
    <th>เงินได้</th>
    <th colspan="2">รายละเอียดการแทง</th>
    <th>ลูกต่อ</th>
    <th>ราคา</th>
</tr>
<?php

$match_date = $result["match_date"];

$query_bill_line = mysql_query("select bill_line.*,match_detail.*  from bill_line inner join match_detail on bill_line.match_detail_id = match_detail.match_detail_id where bill_line.bill_id = '".$bill_id."' and match_detail.match_date = '".$match_date."'; ",$conn);

//สำหรับตรวจสอบว่า บันทึกผลครบทุกคู่หรือยัง
$flag_can_pay = true;

for($i=1; $i<=mysql_num_rows($query_bill_line); $i++){
	$result_line = mysql_fetch_array($query_bill_line);
	
	$AB_HL_DS = $result_line["teamOfTheMatch"];
	$gold = $result_line["bill_line_sub_stageIndex"];	
		
		if($gold == "A" || $gold == "B"){
			$team_display = $result_line["team_".$gold."_name"];
			if($AB_HL_DS == "-") $txt_state = "-";
			else if($gold == $AB_HL_DS) $txt_state = "ต่อ";
			else $txt_state = "รอง";
		}else{//1
		    $team_display = $result_line["team_A_name"];	
			switch($gold){
				case "H": $txt_state = "สูง" ; break;
				case "L": $txt_state = "ต่ำ" ; break;
				case "D": $txt_state = "คู่" ; break;
				case "S": $txt_state = "คี่" ; break;			
			}//end switch();
		}//end else 1;

							//รูปภาพ
	//--------------------------------------------------------------------------------//
	$stage_status = explode(",",$result_line["bill_line_paymentTerm"]);
	switch($stage_status[0]){
		case "0": $img_stage1='<img src="image/false.png" width="15" height="15" />' ; break; //ตาย
		case "1": $img_stage1='<img src="image/o.png" width="15" height="15" />' ; break; //เสมอ
		case "2": $img_stage1='<img src="image/true.png" width="15" height="15" />' ; break; //ชนะ
		default :  $img_stage1 = "";
	}
	switch($stage_status[1]){
		case "0": $img_stage2='<img src="image/false.png" width="15" height="15" />' ; break; //ตาย
		case "1": $img_stage2='<img src="image/o.png" width="15" height="15" />' ; break; //เสมอ
		case "2": $img_stage2='<img src="image/true.png" width="15" height="15" />' ; break; //ชนะ
		default :  $img_stage2 = "";	
	}
	//--------------------------------------------------------------------------------//
	
$score = "";
if($result_line["match_cancelFlag"] != "Y"){//ถ้าไม่ยกเลิก
	if(($result_line["score_team_A"] == "" ) || ($result_line["score_team_B"] == "")) $flag_can_pay = false;
	
	//set ผลบอล
	$score = $result_line["score_team_A"]."&nbsp;-&nbsp;".$result_line["score_team_B"];
}else $score = '<font size="2">ยกเลิก</font>';

?>
<tr height="22" align="center" bgcolor="#FFFFFF">
	<td width="55"><?=$score?></td>
    <td width="55"><?=$img_stage1.",".$img_stage2?></td>
    <td width="60"><?=$result_line["bill_line_pay"]?></td>
    <td width="45"><?=$txt_state?></td>
    <td align="left">(<b><?=$result_line["number"]?></b>)&nbsp;<?=$team_display?></td>
    <td width="55"><?=$result_line["STG_score"]?></td>
    <td width="50"><?=$result_line["STG_rate"]?></td>
</tr>

<?php

} //end loop for();


mysql_close($conn);

if($flag_can_pay == true) $money_pay = $result["pay_amount"];
else $money_pay = 0;
?>

<tr height="40" bgcolor="#2E2E2E" align="center">
<td colspan="7">
&nbsp;<b><font color="#FFFFFF" size="5">เงินได้สุทธิ&nbsp;&nbsp;&nbsp;<?=number_format($money_pay)?>&nbsp;&nbsp;บ.</font></b>
</td>
</tr>
</table>
<br/>

</center>

<br/>
<br/>

<input type="hidden" id="txtChkPay"  value="<?=$money_pay?>" />
</body>
</html>