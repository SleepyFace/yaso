<?phpheader("Content-Type:text/html; charset=utf-8");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
<?php
require_once("condb.php");
/*
$query = mysql_query("select * from score_rate order by IDScoreRate desc limit 1;",$conn);
$result = mysql_fetch_array($query);
$max_score = substr($result["ScoreNmrRate"], (strlen($result["ScoreNmrRate"]) -1));

$query = mysql_query("SELECT MIN(PriceNmrRate) as min_n, MAX(PriceNmrRate) as max_n FROM price_rate;",$conn);
$result = mysql_fetch_array($query);
$rate_min = $result["min_n"];
$rate_max = substr($result["max_n"],0,1);

$query = mysql_query("select * from price_rate limit 2;",$conn);
$result = mysql_fetch_array($query);
$rate1 = $result["PriceNmrRate"];
$result = mysql_fetch_array($query);
$rate2 = $result["PriceNmrRate"];
$rate = $rate2 - $rate1;
*/
?>

<div style="width:100%; height:30px;  padding-top:10px; text-align:center; font-size:14px;">ตั้งค่าลูกต่อสูงสุด และ ตั้งค่าราคา</div>

<br/><br/>
<div style="width:500px; height:auto;">
<table align="center" cellpadding="1" cellspacing="0" border="1" bgcolor="#FFFFFF" bordercolor="#DFDFDF">
<tr height="40" style=" background-image:url(image/title_bg.png); font-size:14px; color:#FFF;">
    <td align="center" colspan="2">ตั้งค่าลูกต่อ</td>
</tr>
<tr>
    <td width="120" align="right" class="bg_img">ลูกต่อสูงสุด : &nbsp;</td>
    <td width="180">
    <select id="sltScore">
    <?php	
    for($i=2; $i<=9; $i++){
		$selected="";
		if($i == 4) $selected = "selected=\"selected\"";
    ?>	
    <option value="<?=$i?>" <?=$selected?> ><?=$i?></option>
    <?php
    }
    
    ?>
    </select>
    </td>
</tr>
<tr>
    <td class="bg_img">&nbsp;</td>
    <td><button type="button" onclick="score_setting();">บันทึกค่า</button></td>
</tr>
</table>

<!-- display saving massage  -->
<br/>
<div style="width:100%; height:25px; text-align:center;"><span id="spLoading" style="display:none; color:#000;"><b>กำลังบันทึก..</b><br/><img src="image/loading.gif" /></span></div>
<br/>

<table align="center" cellpadding="1" cellspacing="0" border="1" bgcolor="#FFFFFF" bordercolor="#DFDFDF">
<tr height="40" style=" background-image:url(image/title_bg.png); font-size:14px; color:#FFF;">
    <td align="center" colspan="2">ตั้งค่าราคาตัวคูณ</td>
</tr>
<tr>
    <td align="right" width="120" class="bg_img">ราคาต่ำสุด :&nbsp;</td>
    <td width="180">
    <select id="sltPriceMIN">
    <option value="1.4">1.4</option>
	<option value="1.5">1.5</option> 
    <option value="1.6" selected="selected">1.6</option>
    <option value="1.7">1.7</option>
    <option value="1.8">1.8</option>
    <option value="1.9">1.9</option>   
   </select></td>
</tr>
<tr>
    <td align="right" class="bg_img">อัตราเพิ่ม :&nbsp;</td>
    <td>
    <select id="sltPriceRate">
        <option value="0.02">0.02</option>
        <option value="0.05" selected="selected">0.05</option>
        <option value="0.1">0.1</option>
    </select></td>
</tr>
<tr>
    <td align="right" class="bg_img">ราคาสูงสุด :&nbsp;</td>
    <td><select id="sltPriceMAX">
        <?php
		$i=2;
        while($i<=9){
        ?>	
        <option value="<?=$i?>"><?=$i?></option>
        <?php
		$i++;
    	 }
        ?>
        </select>
    </td>
</tr>

<tr>
    <td class="bg_img">&nbsp;</td>
    <td><button type="button" onclick="price_setting()">บันทึกค่า</button></td>
</tr>
</table>

</div>



<?php
mysql_close($conn);
?>
</body>
</html>
