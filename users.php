<?phpheader("Content-Type:text/html; charset=utf-8");
	@session_start();
	if(!isset($_SESSION["uname"])) echo '<script type="text/javascript">window.location="front_login.php";</script>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Users</title>
<script type="text/javascript" src="ajax_framework.js"></script>
<script type="text/javascript">
<!--
function menu_OnmouseOver(el){
	el.style.backgroundColor = '#FFFF66';
}
function menu_OnmouseOut(el){
	el.style.backgroundColor = '';
}

function open_page(url){
	url+="?dummy=" + (new Date()).getTime();
	window.open(url,'ifrmFront','');
}

function change_passwd(){
	if(document.getElementById("txtOldpasswd").value == ""){
		document.getElementById("txtOldpasswd").focus();
		return false;
	}
	else if(document.getElementById("txtnewpasswd1").value =="" || document.getElementById("txtnewpasswd2").value == ""){
		if(document.getElementById("txtnewpasswd1").value == "") document.getElementById("txtnewpasswd1").focus();
		else  document.getElementById("txtnewpasswd2").focus();
		return false;
	}
	else if(document.getElementById("txtnewpasswd1").value != document.getElementById("txtnewpasswd2").value){
		alert("รหัสใหม่ไม่ตรงกัน !!.");
		document.getElementById("txtnewpasswd2").focus();
		return false;
	}
	
	var pwd_new = document.getElementById("txtnewpasswd2").value;
	if(pwd_new.length < 4){
			alert("รหัสผ่านต้องมีอย่างน้อย 4 ตัว !!.");
			document.getElementById("txtnewpasswd1").value = "";
			document.getElementById("txtnewpasswd2").value = "";
			document.getElementById("txtnewpasswd1").focus();
			return false;
	}
	
	var data =  "pwd_old=" + document.getElementById("txtOldpasswd").value;
	data += "&pwd_new=" + pwd_new;
	data += "&uname=" + document.getElementById("lbuname").innerHTML;
	fmchg_pwd.reset();
	ajaxLoad('post','change_pwd_ss.php',data,'divWinchangePWD');

}

-->
</script>

<style type="text/css">
<!--
body{
	font-family:Tahoma;
	font-size:14px;
	margin:0px;
	background-color:#444;
	
	}
.menu_right_border{
	border-right:solid 1px #FFF;
	cursor:pointer;
}
-->
</style>
</head>

<body>
<?php
$uid = $_SESSION["uname"];
$pivcode = $_SESSION["front_pivcode"];
?>
<br/>
<div style="width:99%; height:6px; background-color:#2A3F55; margin:auto;"></div>
<table align="center" width="99%" cellpadding="1" cellspacing="0" border="0">
<tr align="center" height="30" valign="top" bgcolor="#2A3F55">
    <td width="135" >
    <img src="image/membersM.png" height="17" width="19" />&nbsp;<font color="#00BFFF" size="3"><label id="lbuname"><?=$uid?></label></font>
    </td>
    
    <td width="150">
    <span style="cursor:pointer; color:#FFFFFF;" onmouseover="this.style.color='#FF9F55';" onmouseout="this.style.color='#FFFFFF';" onclick="document.getElementById('divWinchangePWD').innerHTML='';  document.getElementById('divWinchangePWD').style.display = 'block'; ajaxLoad('post','change_pw.php','','divWinchangePWD');"><img src="image/user_edit.png" /> เปลี่ยนรหัสผ่าน</span>
    
<div id="divWinchangePWD" style="width:260px; height:auto; color:#000; position:absolute; display:none; z-index:1000; border:solid 8px #FFF; background-color:#EBEBEB;"></div>
    </td>
    <td width="180">
    <span style="cursor:pointer; color:#FFFFFF;" onmouseover="this.style.color='#FF9F55';" onmouseout="this.style.color='#FFFFFF';"  onclick="window.open('table/index.php?useMyTable=Y&pivcode=<?=$pivcode?>','','')"><img src="image/print.gif" /> พิมพ์ราคาออกเอง</span>
    </td>
    
    <td>&nbsp;</td>

    <td valign="baseline" width="120" align="right"><span style=" color:#FFF; font-size:14px; cursor:pointer;"  onmouseover="this.style.color='#FF9F55';" onmouseout="this.style.color='#FFFFFF';"  onclick="window.location='logout_ss.php';"><img src="image/unlock.png" /> Logout</span>&nbsp;&nbsp;
    </td>
    
</tr>
</table>


<div style="width:99%; height:auto; margin:auto; border:solid 0px #FFF;">

<div style=" width:100%; height:auto; border-top:solid 3px #FFFFFF;">

<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr  valign="middle" align="center" height="35" bgcolor="#AABFFF">
    <td width="200" align="left"></td>
    <td width="140" bgcolor="#A6B5FD" onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);"  class="menu_right_border" onclick="open_page('front.php');"><img src="image/ball24.png" />ตารางบอลวันนี้</td>
 
    <td width="140" bgcolor="#A6B5FD"  onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);"  class="menu_right_border" onclick="open_page('view-result.php');"><img src="image/ball24-2.png" />ผลบอลย้อนหลัง</td>
     
    <td width="130" bgcolor="#A6B5FD"  onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);"  class="menu_right_border" onclick="open_page('bill_payment.php');"><img src="image/pay24.png" width="24" height="24" />บันทึกจ่ายเงิน</td>
  
    <td width="130" bgcolor="#A6B5FD"  onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);"  class="menu_right_border"  onclick="var win_prtprepay=window.open('print_bill_prepay.php?uid=<?=$uid?>','bill_prtprepay','width=300,height=510,left=450,top=150, resizable=no,scrollbars=auto,status=no,location=no,menubar=no,titlebar=no,toolbar=no');"><img src="image/ball24-3.png" />บิลเตรียมจ่าย</td>
 
    <td width="130" bgcolor="#A6B5FD"  onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);"  class="menu_right_border" onclick="open_page('bill.php');"><img src="image/bill24.png" />รายการบิล</td>

    <td bgcolor="#A6B5FD"  width="120" onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);"  onclick="open_page('report_user.php');" style="cursor:pointer;"><img src="image/ball24-6.png" />รายงานสรุป</td>

<td>&nbsp;</td>
</tr>

</table>
</div>

<iframe id="ifrmFront" name="ifrmFront"  width="100%" frameborder="0" scrolling="auto"></iframe>



</div>

</body>
</html>

<script type="text/javascript">
<!--
document.getElementById("ifrmFront").style.height  = (screen.availHeight - 130) + "px";
open_page('front.php');
-->
</script>