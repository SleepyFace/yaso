<?phprequire_once("header-text-html.php");
	@session_start();
	if(!isset($_SESSION["sadmin"])) echo '<br/><br/><center>กรุณา Login ใหม่ !!</center>';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sub Admin</title>
<script type="text/javascript" src="ajax_framework.js"></script>
<script type="text/javascript">
<!--
function display_schedule(){
	var data = "sltMonth=" + document.getElementById("sltMonth").value;
	data += "&sltYear=" + document.getElementById("sltYear").value;
	ajaxLoad('post','matchManagementDisplay_subAdmin.php',data,'divmatchManageDisplay');
}

function insertMatchDate(){
  var data = "matchDate=" + document.getElementById('txtMatchDate').value;
  	data += "&sltMonth=" + document.getElementById("sltMonth").value;
	data += "&sltYear=" + document.getElementById("sltYear").value;
	
  ajaxLoad('post','matchInsert.php',data,'divmatchManageDisplay');	

}

function setActive(match_date){
	
	if(document.getElementById("txtChkAct").value == 1){
		alert("ไม่อนุญาติ :: ขณะนี้มีตารางการแข่งขัน Active อยู่ ให้ทำการบันทึกผลแข่งขันก่อน !!.");
		return false;
		
	}else if(confirm("คุณต้องการ SetActive ให้เริ่มมีการเปิดแทงตารางนี้ ใช่ หรือ ไม่ ?.")){
		
		document.getElementById("divLoading").style.top = event.clientY + "px";
		document.getElementById("divLoading").style.left = (event.clientX - 60) + "px";
		document.getElementById("divLoading").style.display = "block";
		
		var data = "match_date=" + match_date;
		ajaxLoad('post','setActive.php',data,'');

	}	

}

function my_getDate(txt){
	document.getElementById("displayCalendar").style.left = event.clientX + "px";
	if(document.getElementById("displayCalendar").style.display=='block'){
		document.getElementById("displayCalendar").style.display='none';
	}
	else{
		document.getElementById("displayCalendar").style.display='block';
		window.open('NAWEE_CALENDAR_1.php?text='+txt,'ifrmNAWEE_CALENDAR','');
	}
}

function view_table(match_status ,match_date){

	if(match_status == "Saved"){
		alert("ตารางนี้บันทึกผลสมบูรณ์แล้ว ไม่อนุญาติให้เปิดดู !!.");
		return false;
	}else{
		window.open('matchPreview.php?dummy=' + (new Date()).getTime() + '&match_date='+match_date + '&tbStatus='+match_status + '&subadmin=Y'  ,'ifrmsAdmin','');
	}

}
-->
</script>

<style type="text/css">
<!--
body{
	font-family:Tahoma;
	font-size:14px;
	margin:3px;
	background-color:#AABFFF;
	color:#000080;
	
	}
.menu_right_border{
	border-right:solid 2px #FFF;
}

input,textarea,select{
	border:solid 2px #FF7F00;
}

button{
	cursor:pointer;
}
-->
</style>
</head>

<body>
<?php
$arr_month= array('','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤษจิกายน','ธันวาคม');
?>

<br />

<!------------------------------------- Main Display ----------------------------------->
<div style="width:820px; height:auto; background-color:#FFF; margin:auto;">

<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
    <td align="right" width="200"><font color="#000080">สร้างวันที่แข่งขัน :</font></td>
    <td><input type="text" id="txtMatchDate"  value="<?=date("Y-m-d")?>" style="text-align:center; cursor:pointer;" readonly="readonly" size="12"  onclick="my_getDate('txtMatchDate')" />
    <div id="displayCalendar" style=" width:185px; height:auto; display:none; position:absolute; z-index:1100">
		<iframe name="ifrmNAWEE_CALENDAR" style="width:100%; height:175px;"  frameborder="0" marginheight="0" marginwidth="0"></iframe>
	</div>
    </td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td><button onclick="insertMatchDate();"><img src="image/add.gif" /> สร้างวันที่แข่ง</button></td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>

<tr height="35" style="color:#3FF; background-image:url(image/title_bg.png);">
    <td align="right">แสดงตารางแข่ง เดือน :</td>
    <td><select id="sltMonth"><?php 
	for($i=1; $i<=12; $i++){
	?>
    <option value="<?=$i?>"><?=$arr_month[$i]?></option>
    
    <?php
		}
	?></select> &nbsp;ปี 
    <select id="sltYear">
   		<option value="<?=date('Y')+1?>"><?=date('Y')+1?></option>
    	<option value="<?=date('Y')?>"><?=date('Y')?></option>
        <option value="<?=date('Y')-1?>"><?=date('Y')-1?></option>
        <option value="<?=date('Y')-2?>"><?=date('Y')-2?></option>
    </select>
    &nbsp;<button type="button" onclick="display_schedule();"><img src="image/search16.png" /> แสดงตาราง</button></td>
</tr>
</table>
<br/>
<div id="divmatchManageDisplay" style="width:100%; height:auto; border:solid 0px #006600;"></div>

<!-- Display Loading-->
<div id="divLoading" style="background-color:#7F0000; color:#FFF; border:solid 4px #FFFFFF; text-align:center; width:170px; height:auto; position:absolute; display:none; z-index:1000;"><b>กำลังทำงาน</b>...<br/><img src='image/loading.gif' /></div>


</div>
<!------------------------------------- END Main Display ----------------------------------->
<br /><br />

</body>
</html>


<script type="text/javascript">
<!--
document.getElementById("sltMonth").selectedIndex = "<?=date('m')-1?>";
document.getElementById("sltYear").selectedIndex  = 1;
display_schedule();
-->
</script>