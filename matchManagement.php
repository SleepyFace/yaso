<?php header("Content-Type:text/html; charset=utf-8");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script type="text/javascript" src="ajax_framework.js"></script>
<script type="text/javascript">
<!--
function display_schedule(){
	var data = "sltMonth=" + document.getElementById("sltMonth").value;
	data += "&sltYear=" + document.getElementById("sltYear").value;
	ajaxLoad('post','matchManagementDisplay.php',data,'divmatchManageDisplay');
}

function insertMatchDate(){
  var data = "matchDate=" + document.getElementById('txtMatchDate').value;
  	data += "&sltMonth=" + document.getElementById("sltMonth").value;
	data += "&sltYear=" + document.getElementById("sltYear").value;
	
  ajaxLoad('post','matchInsert.php',data,'divmatchManageDisplay');	

}

function setOpen(match_date){ 
	if(document.getElementById("txtChkAct").value == 1){
		alert("มีตารางเปิด Active อยู่ ต้องบันทึกผลตารางนั้นก่อน !.");
		return false;
		
	}else if(confirm("ต้องการ เปิดให้แทงตารางนี้ ใช่ หรือ ไม่ ?.")){
		
		document.getElementById("divLoading").style.top = (event.clientY-10) + "px";
		document.getElementById("divLoading").style.left = (event.clientX-10) + "px";
		document.getElementById("divLoading").style.display = "block";
		
		var data = "match_date=" + match_date;
		ajaxLoad('post','setActive.php',data,'');

	}	

}
	/*
function setActive(match_date){
	

	if(document.getElementById("txtChkAct").value == 1){
		alert("ไม่อนุญาติ :: ขณะนี้มีตารางการแข่งขัน Active อยู่ ให้ทำการบันทึกผลแข่งขันก่อน !!.");
		return false;
		
	}else if(confirm("คุณต้องการ SetActive ให้เริ่มมีการเปิดแทงตารางนี้ ใช่ หรือ ไม่ ?.")){
		
		document.getElementById("divLoading").style.top = event.clientY + "px";
		document.getElementById("divLoading").style.left = (event.clientX - 60) + "px";
		document.getElementById("divLoading").style.display = "block";
		
		var data = "match_date=" + match_date;
		ajaxLoad('post','setActive.php',data,'');

	}	
	
	alert();

}
*/
function reverse_table(match_date){
	var data = "match_date=" + match_date;
	ajaxLoad('post','reverse_table.php',data,'divmatchManageDisplay');
}

function my_getDate(txt){
	document.getElementById("displayCalendar").style.left = (event.clientX-61) + "px";
	if(document.getElementById("displayCalendar").style.display=='block'){
		document.getElementById("displayCalendar").style.display='none';
	}
	else{
		document.getElementById("displayCalendar").style.display='block';
		window.open('NAWEE_CALENDAR_1.php?text='+txt,'ifrmNAWEE_CALENDAR','');
	}
}

-->
</script>
<style type="text/css">
<!--
body{
	font-family:Tahoma;
	font-size:14px;
	margin:0px;
	background-color:#AABFFF;
	
	}
.menu_right_border{
	border-right:solid 2px #FFF;
}

input,textarea,select{
	border:solid 1px #FF7F00;
	height:20px;
}

button{
	cursor:pointer;
}
-->
</style>

</head>

<body>
<br/>
<center><div style="width:350px; height:25px; background-color:#7F9FFF; padding-top:4px; border:solid 1px #009FFF;">จัดการตารางแข่งขัน</div></center>
<?php
$arr_month= array('','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤษจิกายน','ธันวาคม');
?>
<br/>

<div style="width:1000px; height:auto; margin:auto; background-color:#FFFFFF;">

<table width="100%" cellpadding="2" cellspacing="1" border="0" bgcolor="#7F9FFF">
<tr bgcolor="#2A5FAA">
    <td align="right" width="200"><font color="#FFFFFF">สร้างวันที่แข่ง :</font></td>
    <td><input type="text" id="txtMatchDate"  value="<?=date("Y-m-d")?>" style="text-align:center; cursor:pointer;" readonly="readonly" size="12"  onclick="my_getDate('txtMatchDate')" />
    
    <button onclick="insertMatchDate();"><img src="image/add.gif" />เพิ่ม</button>
    </td>
</tr>

<tr height="4" bgcolor="#2A3FAA">
    <td></td>
    <td><div id="displayCalendar" style=" width:185px; height:auto; display:none; position:absolute; z-index:1100">
		<iframe name="ifrmNAWEE_CALENDAR" style="width:100%; height:175px;"  frameborder="0" marginheight="0" marginwidth="0"></iframe>
	</div>
</td>
</tr>

<tr height="35" align="right"  bgcolor="#D4DFFF">
    <td align="center"><span onclick="window.open('tb_comment.php','','');"><font color="#D40000" style="cursor:pointer;"><b><u>แก้ข้อความ(ท้ายโพย)</u></b></font></span></td>
    <td>แสดงตารางแข่ง เดือน :<select id="sltMonth"><?php 
	for($i=1; $i<=12; $i++){
	?>
    <option value="<?=$i?>"><?=$arr_month[$i]?></option>
    
    <?php
		}
	?></select> &nbsp;ปี 
    <select id="sltYear">
   		<option value="<?=date('Y')+1?>"><?=date('Y')+1?></option>
    	<option value="<?=date('Y')?>"><?=date('Y')?></option>
        <option value="<?=date('Y')-1?>"><?=date('Y')-1?></option>
        <option value="<?=date('Y')-2?>"><?=date('Y')-2?></option>
    </select>
    &nbsp;<button type="button" onclick="display_schedule();"> แสดงตาราง</button></td>
</tr>
</table>

<br/>
<div id="divmatchManageDisplay" style="width:100%; height:auto; border:solid 0px #006600;"></div>

</div>

<!-- Display Loading-->
<div id="divLoading" style="width:80px; height:auto; position:absolute; display:none; z-index:1000;"><img src='image/loading3.gif' /></div>



</body>
</html>

<script type="text/javascript">
<!--
document.getElementById("sltMonth").selectedIndex = "<?=date('m')-1?>";
document.getElementById("sltYear").selectedIndex  = 1;
display_schedule();
-->
</script>
