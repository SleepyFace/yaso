<?php require_once("header-text-html.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>แก้ทีมต่อ</title>
<script type="text/javascript">
<!--
function edit_termOfmatch(){
	if(document.getElementById("txtmatchDate").value ==""){
		alert('ไม่มีวันที่ตาราง !!.');
		return false;
	}
	if(document.getElementById("sltTeamCode").value == ""){
		alert('กรุณาระบุรหัสทีม !!.');
		document.getElementById("sltTeamCode").focus();
		return false;
	}
	else if(document.getElementById("txtTeamA").value == "" || document.getElementById("txtTeamB").value == ""){
		alert('ชื่อทีมต้องไม่เป็นค่าว่าง กรุณาเลือกรหัสทีมใหม่ !!.');
		document.getElementById("sltTeamCode").selectedIndex = 0;
		document.getElementById("txtTeamA").value ="";
		document.getElementById("txtTeamB").value ="";
		document.getElementById("sltTeamCode").focus();
		return false;
	}
	
fm.submit();	
}

function setTXTteam(){
	if(document.getElementById("sltTeamCode").value != ""){
	  var id = document.getElementById("sltTeamCode").value;
	  document.getElementById("txtTeamA").value = window.opener.document.getElementById("tdA"+ id).innerHTML;
	  document.getElementById("txtTeamB").value = window.opener.document.getElementById("tdB"+ id).innerHTML;
	}	
	
}
	
	
-->
</script>
<style type="text/css">
<!--
body{
	font-family:Tahoma, Geneva, sans-serif;
	font-size:14px;
	margin:0px;
	}

-->
</style>
</head>

<body onblur="window.close();">
<form name="fm" method="post" action="termOfmatch_edit_ss.php" target="_self">
<input type="hidden" value="<?=$_GET['matchDate']?>" id="txtmatchDate" name="txtmatchDate" />

    <div style="background-color:#007FAA; width:100%; height:35px; font-size:18px; color:#FFF;">
        <b><center>แก้ไขสถานะทีมต่อ ชื่อทีม</center></b>
    </div>


<font color="#D40000">
<b><u>แจ้งเตือน</u> </b>: <br/>
1).การแก้ทีมต่อ จะมีผลกับการคิดเงินของบิลทุกใบที่แทงทีมนี้ ให้ใช้เฉพาะกรณี ที่ set ทีมต่อผิด เท่านั้น
<br/>
2).ห้ามใช้กับกรณีคู่บอล ที่ราคาไหลจนต้องเปลี่ยนทีมต่อ กรณีนี้ยังไม่ได้ข้อสรุป ให้ลีกเลี่ยงเปิดคู่บอลที่มีลักษณะแบบนี้
<br/><br/>
</font>

<table width="400" align="center" cellpadding="2" cellspacing="2" border="0" bgcolor="#AADFAA">
<tr align="center" height="25" bgcolor="#FFBFFF">
    <td colspan="2"><b>แก้ไขทีมต่อ</b></td>
</tr>

<tr bgcolor="#D4FFFF">
<td align="right" width="150">ระบุรหัสทีม</td><td align="left"><select id="sltTeamCode" name="sltTeamCode" onchange="setTXTteam();">	<option value=""></option>
<?php
for($i=1; $i<=99; $i++){

?>
<option value="<?=$i?>">R<?=$i?></option>
<?php
}
?>
</select>
<font color="#FF0000"><b>*</b></font>
</td>
</tr>
<tr bgcolor="#D4FFFF"><td align="right">แก้ทีมต่อ เป็น</td>
<td align="left"><select id="sltTermOfmatch" name="sltTermOfmatch">
<option value=""></option>
<option value="A">เจ้าบ้าน</option>
<option value="B">ทีมเยือน</option>
</select>
&nbsp;<font color="#FF0000" size="2">*ไม่แก้ ก็ไม่ต้องใส่ครับ</font>
</td>
</tr>
<tr align="center" height="25" bgcolor="#FFBFFF">
    <td colspan="2"><b>แก้ไขชื่อทีม</b></td>
</tr>
<tr bgcolor="#D4FFFF">
    <td  align="right">ทีมเจ้าบ้าน</td>
    <td><input type="text" id="txtTeamA" name="txtTeamA" /><font color="#FF0000"><b>*</b></font></td>
</tr>
<tr bgcolor="#D4FFFF">
    <td  align="right">ทีมเยือน</td>
    <td><input type="text" id="txtTeamB" name="txtTeamB" /><font color="#FF0000"><b>*</b></font></td>
</tr>

<tr><td></td><td><button type="button" style="width:100px; height:35px; cursor:pointer;" onclick="edit_termOfmatch();">บันทึก</button></td></tr>
</table>


</form>
</body>
</html>