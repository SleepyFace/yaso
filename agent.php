<?php require_once("header-text-html.php");
	@session_start();
	if(!isset($_SESSION["pivcode"])) echo '<script type="text/javascript">window.location="report_login.php";</script>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agent</title>
<script type="text/javascript" src="ajax_framework.js"></script>
<script type="text/javascript">
<!--
function menu_OnmouseOver(el){
	el.style.backgroundColor = '#FFFF66';
	el.style.color = '#2A3F55';
}
function menu_OnmouseOut(el){
	el.style.backgroundColor = '';
	el.style.color = '#FFFFFF';
}

function open_page(url){
	url += "?dummy="+(new Date()).getTime();
	url += "&pivcode=<?=$_SESSION["pivcode"]?>"
	window.open(url,'ifrmPIVadmin','');

}


function change_passwd(){
	if(document.getElementById("txtOldpasswd").value == ""){
		document.getElementById("txtOldpasswd").focus();
		return false;
	}
	else if(document.getElementById("txtnewpasswd1").value =="" || document.getElementById("txtnewpasswd2").value == ""){
		if(document.getElementById("txtnewpasswd1").value == "") document.getElementById("txtnewpasswd1").focus();
		else  document.getElementById("txtnewpasswd2").focus();
		return false;
	}
	else if(document.getElementById("txtnewpasswd1").value != document.getElementById("txtnewpasswd2").value){
		alert("รหัสใหม่ไม่ตรงกัน !!.");
		document.getElementById("txtnewpasswd2").focus();
		return false;
	}
	
	var pwd_new = document.getElementById("txtnewpasswd2").value;
	if(pwd_new.length < 4){
			alert("รหัสผ่านต้องมีอย่างน้อย 4 ตัว !!.");
			document.getElementById("txtnewpasswd1").value = "";
			document.getElementById("txtnewpasswd2").value = "";
			document.getElementById("txtnewpasswd1").focus();
			return false;
	}
	
	var data =  "pwd_old=" + document.getElementById("txtOldpasswd").value;
	data += "&pwd_new=" + pwd_new;
	data += "&uname=<?=$_SESSION["pivcode"]?>";
	fmchg_pwd.reset();
	ajaxLoad('post','piv_change_pwd.php',data,'divWinchangePWD');

}


-->
</script>

<style type="text/css">
<!--
body{
	font-family:Tahoma;
	font-size:14px;
	margin:4px;
	background-color:#444;
	
	}
.menu_right_border{
	border-right:solid 1px #FFF;
	cursor:pointer;
	color:#FFFFFF;
}
.menu_right_border2{
	cursor:pointer;
	color:#FFFFFF;
}
-->
</style>
</head>

<body>

<div style="width:99%; height:auto; margin:auto;">

<div style="width: 100%; height: auto; background-color:#CCC;">

<div style="width:100%; height:5px; background-color:#666; border-bottom:solid 2px #FFFFFF;"></div>

<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr align="center" bgcolor="#2A3FAA" height="40">
        <td width="50" align="left">&nbsp;&nbsp;</td>
        
        <td width="110" class="menu_right_border" onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);" onclick="open_page('index_piv2.php');"><img src="image/ball24.png" />ตารางแข่งขัน</td>
        
        <td width="120" class="menu_right_border" onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);" onclick="open_page('rate/index.php');"><img src="image/edit24.png" />ปรับราคา</td><!--  สรุปยอดขาย   การรับจ่ายเงิน  -->
                
        <td width="120" class="menu_right_border" onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);">
            <span onclick="document.getElementById('divWinchangePWD').innerHTML='';  document.getElementById('divWinchangePWD').style.display = 'block'; ajaxLoad('post','change_pw.php','','divWinchangePWD');">&nbsp;<img src="image/key.png" />เปลี่ยนรหัส&nbsp;</span>
            <div id="divWinchangePWD" style="width:260px; height:auto; position:absolute; display:none; z-index:1000; border:solid 8px #FFF; background-color:#EBEBEB;"></div>
        </td>
        
        <td width="130" class="menu_right_border" onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);" onclick="open_page('agent_setting.php');"><img src="image/Settings24.png" />ตั้งค่าการแทง</td>
        
        <td width="120" class="menu_right_border" onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);" onclick="open_page('piv_user_info.php');"><img src="image/user.png" />ข้อมูลผู้ใช้</td>
       
        <td width="120" class="menu_right_border" onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);" onclick="open_page('reports/statistic_index-piv_rpt.php');"><img src="image/chart-icon.png" />สถิติยอดแทง </td><!--  สรุปยอดขาย   การรับจ่ายเงิน  -->
       
        <td width="120" class="menu_right_border2" onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);" onclick="open_page('index_report.php');"><img src="image/ball24-6.png" />รายงานสรุป </td><!--  สรุปยอดขาย   การรับจ่ายเงิน  -->
 

     <td>&nbsp;</td>
   
    <td align="right"  width="220" valign="baseline" style="color:#D4FFFF;">
    <img src="image/userlogin.png" /> Agent Login :<br/><b><label id="lbuname"><?=$_SESSION["pivcode"]."--".$_SESSION["pivname"]."--"?></label></b>&nbsp;
    </td>
    
    <td valign="baseline" width="100" align="right"><span style=" color:#FFBF00; cursor:pointer;" onmouseover="this.style.color='#D40000';" onmouseout="this.style.color='#FFBF00';" onclick="window.location='logout_report_ss.php';"><b><img src="image/logout.png" /> Logout</b></span>&nbsp;&nbsp;</td>

    </tr>
</table>

</div>

<div style="width:100%; height:10px; background-color:#FFF;"></div>

<iframe id="ifrmPIVadmin" name="ifrmPIVadmin"  width="100%" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto"></iframe>



</div>

<div id="divMainLoading" style=" width:220px; height:auto; background-color:#F5F5F5; border:solid 8px #7F0000; position:absolute; display:none; top:270px; text-align:center; z-index:2000;">
	<h3><img src="image/loading.gif" /><br/><span id="spTextLoad"></span></h3>
</div>

</body>
</html>

<script type="text/javascript">
<!--
document.getElementById("ifrmPIVadmin").style.height  = (screen.availHeight - 120)+ "px";
open_page('index_piv2.php?pivcode=<?=$_SESSION["pivcode"]?>');
-->
</script>