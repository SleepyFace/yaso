<?phprequire_once("../header-text-html.php");
@session_start();
if(!isset($_SESSION["piv_adjustCode"])){
		echo "<script type='text/javascript'>window.location = 'pivAdjust_login.php';</script>";
		exit(0);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ปรับลูกต่อ ราคา</title>
<script type="text/javascript" src="../ajax_framework.js"></script>
<script type="text/javascript">
<!--
function adjust(match_detail_id){
	var data = "match_date=" + document.getElementById("lbMatchDate").innerHTML ;
	data+="&match_detail_id=" + match_detail_id;
	data+="&pivcode=" + document.getElementById("txtpivcode").value;

	ajaxLoad('post','pivAdjust.php',data,'divDisplayAdjust');
	
	var divDisplayAdjust = document.getElementById("divDisplayAdjust");
	divDisplayAdjust .innerHTML = "";
	divDisplayAdjust.style.left = ((screen.width / 2) - 300) + "px";
	divDisplayAdjust.style.top = (document.body.scrollTop + 50) + "px";
	divDisplayAdjust.style.display = "block";	
}
	
function submit_adjust(match_id){
	var data = "match_id=" + match_id;
	data+="&match_date=" + document.getElementById("lbMatchDate").innerHTML;
	data+="&pivcode=" + document.getElementById("txtpivcode").value;
		
	data += "&stg1=" + document.getElementById("sltSTG1").value.replace("+","p");
	data += "&stg1A=" + document.getElementById("sltRateSTG1_A").value;
	data += "&stg1B=" + document.getElementById("sltRateSTG1_B").value;
	
	data += "&stg2=" + document.getElementById("sltSTG2").value.replace("+","p");
	data += "&stg2A=" + document.getElementById("sltRateSTG2_A").value;
	data += "&stg2B=" + document.getElementById("sltRateSTG2_B").value;
	
	data += "&stgsumscore=" + document.getElementById("sltSTGsumScore").value.replace("+","p");
	data += "&stgH=" + document.getElementById("sltRateSTGH").value;
	data += "&stgL=" + document.getElementById("sltRateSTGL").value;
	data += "&stgD=" + document.getElementById("sltRateSTGD").value;
	data += "&stgS=" + document.getElementById("sltRateSTGS").value;
	
	document.getElementById("divDisplayAdjust").innerHTML = "<br><center><b>กำลังบันทึก</b>..<br/><img src='../image/loading3.gif' /></center><br/>";
	ajaxLoad('post','pivAdjust_submit.php',data,'divDisplayAdjust');
	
}

/*
function change_passwd(){
	if(document.getElementById("txtOldpasswd").value == ""){
		document.getElementById("txtOldpasswd").focus();
		return false;
	}
	else if(document.getElementById("txtnewpasswd1").value =="" || document.getElementById("txtnewpasswd2").value == ""){
		if(document.getElementById("txtnewpasswd1").value == "") document.getElementById("txtnewpasswd1").focus();
		else  document.getElementById("txtnewpasswd2").focus();
		return false;
	}
	else if(document.getElementById("txtnewpasswd1").value != document.getElementById("txtnewpasswd2").value){
		alert("รหัสใหม่ไม่ตรงกัน !!.");
		document.getElementById("txtnewpasswd2").focus();
		return false;
	}
	
	var pwd_new = document.getElementById("txtnewpasswd2").value;
	if(pwd_new.length < 4){
			alert("รหัสผ่านต้องมีอย่างน้อย 4 ตัว !!.");
			document.getElementById("txtnewpasswd1").value = "";
			document.getElementById("txtnewpasswd2").value = "";
			document.getElementById("txtnewpasswd1").focus();
			return false;
	}
	
	var data =  "pwd_old=" + document.getElementById("txtOldpasswd").value;
	data += "&pwd_new=" + pwd_new;
	data += "&chge_pivcode=" + document.getElementById("txtpivcode").value;
	fmchg_pwd.reset();
	ajaxLoad('post','change_pass_ss.php',data,'divWinchangePWD');

}
*/
-->
</script>
<style type="text/css">
<!--
body{
	font-family:Tahoma;
	font-size:13px;
	margin:3px;
	background-color:#069;
	color:#000080;
	
	}
	
.menu_right_border{
	border-right:solid 2px #FFF;
}

input,textarea,select{
	border:solid 2px #FF7F00;
}

button{
	cursor:pointer;
}

#td_{
	color:#FFF; 
	font-weight:bold; 
	background-image:url(../image/title_bg.png); 
	background-repeat:repeat-x;
}
-->
</style>

</head>

<body>
<?php
require_once("../condb.php");

$pivcode = $_SESSION["piv_adjustCode"];
$uid = $pivcode;
?>
<div style="width:98%; height:auto; border:solid 3px #FFF;  margin:auto; background-color:#AABFFF;">


<div style=" width:100%; height:auto; background-color:#7F9FFF;">

<div style="width:100%; height:10px; background-color:#666; border-bottom:solid 2px #FFFFFF;"></div>

<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr align="center" height="25">
        <td width="265" align="left"></td>
        <td><input type="hidden" id="txtpivcode" value="<?=$pivcode?>" /></td>
        <td>&nbsp;</td>
        <td width="200" valign="baseline"><span style="cursor:pointer;" onclick="window.open('../table/index.php?useMyTable=Y&pivcode=<?=$pivcode?>','','')"><u><img src="../image/print.gif" />พิมพ์ราคาออกเอง</u></span></td>
<!--
       <td valign="baseline" align="right" width="110">
    <span style="color:#03F; cursor:pointer;" onclick="document.getElementById('divWinchangePWD').innerHTML='';  document.getElementById('divWinchangePWD').style.display = 'block'; ajaxLoad('post','change_pass.php','','divWinchangePWD');"><img src="../image/key16.png" />เปลี่ยนรหัส</span>
    
<div id="divWinchangePWD" style="width:260px; height:auto; position:absolute; display:none; z-index:1000; border:solid 8px #FFF; background-color:#EBEBEB;"></div>
       </td>
        
        <td align="right" valign="baseline" width="250">
        <img src="../image/userlogin.png" />Agent Login :&nbsp;<font color="#0000FF"><b><label id="lbuname"><?//$uid?></label></b><br/><?//"(".$_SESSION["adjust_pivname"].")"; ?></font>
        </td>
  
        <td valign="baseline" width="100" align="right"><span style=" color:#D40000; cursor:pointer;" onmouseover="this.style.color='#FFBF00';" onmouseout="this.style.color='#D40000';" onclick="window.location='pivAdjust_logout.php';"><b><img src="../image/logout.png" /> Logout</b></span>&nbsp;&nbsp;</td>
 -->	 
    </tr>
    
</table>

</div>



<?php

$query_matchDate = mysql_query("select match_date from match_h where matchStatus = 'Active';",$conn);
if(mysql_num_rows($query_matchDate) < 1){
	mysql_close($conn);
	echo "<br/><br/><br/><center>ยังไม่มีตาราง Active ในขณะนี้ !!.</center>";
	exit(0);
}else{
	$result_matchDate = mysql_fetch_array($query_matchDate);
	$match_date =$result_matchDate["match_date"];
}

$sql="SELECT m1.match_cancelFlag,
m1.match_detail_id,
m1.match_date,
m1.match_time,
m1.teamOfTheMatch,
m1.team_A_id,
m1.team_A_name,
m1.team_B_id,
m1.team_B_name,
m1.league_id,
m1.league_name,
m1.No_STG1A,
m1.No_STG1B,
m1.No_STG2A,
m1.No_STG2B,
m1.No_STGHigh,
m1.No_STGLow,
m1.No_STGDual,
m1.No_STGSingle,
m1.score_team_A,
m1.score_team_B ,

m2.score_STG1,
m2.rate_STG1_A,
m2.rate_STG1_B,
m2.score_STG2,
m2.rate_STG2_A,
m2.rate_STG2_B,
m2.score_sumSTG,
m2.rate_STGHigh,
m2.rate_STGLow,
m2.rate_STGDual,
m2.rate_STGSingle

FROM match_detail m1 INNER JOIN piv_match_detail m2 ON m1.match_detail_id = m2.match_detail_id 
AND m1.match_date = m2.match_date 
WHERE m1.match_date = '".$match_date ."'  AND m2.piv_code = '".$pivcode."'
ORDER BY m1.league_id, m1.match_detail_id asc; ";
$query = mysql_query($sql,$conn);
 
?>

<br/>
<div style="width:1180px; height:auto; margin:auto; background-color:#F5F5F5;">
<br/>
<div style="width:1120px; height:auto; background-color:#FFF; margin:auto; border:solid 4px #FFFFFF;">

<center><br/><font size="+2">ปรับราคา ตารางแข่ง วันที่ <label id="lbMatchDate"><?=$match_date?></label></font><br/></center>
<br/>
<table width="100%" cellpadding="2" cellspacing="1" border="0" bgcolor="#AABFFF">
<tr align="center" height="40" style="color:#FFF;" bgcolor="#3366CC">  
	<td><font color="#FFBF55"><b>ปรับ<br/>ราคา</b></font></td>
    <td>เวลา</td>
    <td>ผลแข่ง</td>
    <td colspan="3" align="right">ทีมเจ้าบ้าน&nbsp;&nbsp;</td>
    <td><u>ลูกต่อ1</u></td>
    <td colspan="3" align="left">&nbsp;&nbsp;ทีมเยือน</td>
    <td colspan="2">เจ้าบ้าน</td>
    <td><u>ลูกต่อ2</u></td>
    <td colspan="2">เยือน</td>
    <td colspan="2">สูง</td>
    <td><u>ป.สูงต่ำ</td>
    <td colspan="2">ต่ำ</td>
    <td colspan="2">คู่</td>
    <td colspan="2">คี่</td>
</tr>

<?php
$league = "";

for($i=1; $i<=mysql_num_rows($query); $i++){
$result = mysql_fetch_array($query);
if($league != $result["league_id"]){
	
?>
<tr align="center" bgcolor="#2A3F55">
    <td colspan="24" align="left">&nbsp;&nbsp;<b><font color="#FFFFFF"><?=$result["league_name"]?></font></b></td>

</tr>
<?php
}
	
	$font_colorA = "";
	$font_colorA2 = "";
	$font_colorB = "";
	$font_colorB2 = "";

if($result["teamOfTheMatch"] == "A"){
	$font_colorA = "<font color='red'>*<u><b>";
	$font_colorA2 = "</b></u></font>";
	$font_colorB = "<font color='blue'>";
	$font_colorB2 = "</font>";
	
}else if($result["teamOfTheMatch"] == "B") {
	$font_colorA = "<font color='blue'>";
	$font_colorA2 = "</font>";
	$font_colorB = "<font color='red'>*<u><b>";
	$font_colorB2 = "</b></u></font>";
}

$id = $result["match_detail_id"];

$score = "";
if($result["match_cancelFlag"] != "Y") $score = "<b>".$result["score_team_A"]."&nbsp;-&nbsp;".$result["score_team_B"]."</b>";
else $score = '<font size="2">ยกเลิก</font>';

?>	
<tr align="center" height="26" bgcolor="#E7EAFE" onmousemove="this.style.backgroundColor = '#FFFF99';" onmouseout="this.style.backgroundColor = '';">
    <td width="50" bgcolor="#F5F5F5" style="cursor:pointer;" onclick=" window.scrollTo(0,0); adjust('<?=$id?>');"><img src='../image/b_edit.png' /><?=$id?></td>

    <td width="42"><?=substr($result["match_time"],0,5)?></td>
    <td width="45" bgcolor="#EAEAEA"><?=$score?></td>
    <td align="right"><?phpecho $font_colorA.$result["team_A_name"].$font_colorA2?>&nbsp;</td>
    <td width="25" style="color:red;"><u><?=$result["No_STG1A"]?></u></td>
    <td width="28" id="tdSTG1_A<?=$id?>"><?=$result["rate_STG1_A"]?></td>
    <td width="42" id="tdSTG1<?=$id?>" style="color:#00F;"  bgcolor="#FFFFCC"><b><?=$result["score_STG1"]?></b></td>
    <td width="28" id="tdSTG1_B<?=$id?>"><?=$result["rate_STG1_B"]?></td>
    <td width="25" style="color:red;"><u><?=$result["No_STG1B"]?></u></td>
    <td align="left">&nbsp;<?phpecho $font_colorB.$result["team_B_name"].$font_colorB2?></td>
   
    <td style="color:red;"><u><?=$result["No_STG2A"]?></u></td>
    <td width="28" id="tdSTG2_A<?=$id?>" ><?=$result["rate_STG2_A"]?></td>
    <td width="42" id="tdSTG2<?=$id?>" style="color:#00F;"  bgcolor="#CCFFFF"><b><?=$result["score_STG2"]?></b></td>
    <td width="28" id="tdSTG2_B<?=$id?>" ><?=$result["rate_STG2_B"]?></td>
    <td style="color:red;"><u><?=$result["No_STG2B"]?></u></td>
    
    <td style="color:red;"><u><?=$result["No_STGHigh"]?></u></td>
    <td width="28" id="tdSTG_H<?=$id?>" ><?=$result["rate_STGHigh"]?></td>
    <td width="50" id="tdSTG_HLDS<?=$id?>" style="color:#00F;" bgcolor="#FFCCFF"><b><?=$result["score_sumSTG"]?></b></td>
    <td width="28" id="tdSTG_L<?=$id?>" ><?=$result["rate_STGLow"]?></td>
    <td style="color:red;"><u><?=$result["No_STGLow"]?></u></td>
    
    <td style="color:red;"><u><?=$result["No_STGDual"]?></u></td>
    <td width="28" id="tdSTG_D<?=$id?>" ><?=$result["rate_STGDual"]?></td>
    <td width="28" id="tdSTG_S<?=$id?>" ><?=$result["rate_STGSingle"]?></td>
    <td style="color:red;"><u><?=$result["No_STGSingle"]?></u></td>
</tr>
<?php
$league = $result["league_id"];

}

mysql_close($conn);
?>
</table>

    </div>
    <br/><br/>
    
</div>
<br/>

</div>

<br/><br/>


<div id="divDisplayAdjust" style="width:500px; height:auto; border:solid 10px #D4DFFF; position:absolute; display:none; background-color:#AABFFF; z-index:1000;"></div>

</body>
</html>