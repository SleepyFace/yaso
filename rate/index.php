<?phprequire_once("../header-text-html.php");
@session_start();
if(!isset($_SESSION["piv_adjustCode"])){
		echo "<script type='text/javascript'>window.location = 'pivAdjust_login.php';</script>";
		exit(0);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ปรับลูกต่อ ราคา</title>
<script type="text/javascript" src="../ajax_framework.js"></script>
<script type="text/javascript">
<!--
function adjustBox_show(stg,id){
		
	var top = (window.pageYOffset || document.documentElement.scrollTop ) + window.event.clientY;
	var left = (window.pageXOffset || document.documentElement.scrollLeft ) + window.event.clientX;

	var adjustBox = document.getElementById("divAdjustBox");
	adjustBox.style.top = top+ "px";
	adjustBox.style.left = left-10+"px";
	
	adjustBox.innerHTML = "";
	var data = "stg=" + stg;
	data+="&m_id=" +id;
	
	var url;
	if(stg=="STG1" || stg=="STG2"  ||  stg=="STGHL" ) url="../quick_display_stg_rate.php"; // ปรับลูกต่อ
	else url="../quick_display_rate.php"; // ปรับราคา
	
	ajaxLoad('post',url,data,'divAdjustBox');
	adjustBox.style.display = "block";
	
}

function adjustRateQuick(master_rate, relation_rate, stg, id){

	var data = "master_rate=" + master_rate;
	data+="&relation_rate=" +relation_rate ;
	data+="&status=piv";
	data+="&pivcode=" + document.getElementById("txtpivcode").value;
	data+="&stg=" + stg;
	data+="&m_id=" + id;
	data+="&match_date=" + document.getElementById("lbMatchDate").innerHTML;
	ajaxLoad('post','../quick_adjust_rate_ss.php',data,'');
	
}

function adjustBox_hidden(){
	var adjustBox = document.getElementById("divAdjustBox");
	adjustBox.style.display = "none";
}


function adjust(match_detail_id){
	var data = "match_date=" + document.getElementById("lbMatchDate").innerHTML ;
	data+="&match_detail_id=" + match_detail_id;
	data+="&pivcode=" + document.getElementById("txtpivcode").value;
		
	var top = (window.pageYOffset || document.documentElement.scrollTop ) + window.event.clientY;
	var left = (window.pageXOffset || document.documentElement.scrollLeft ) + window.event.clientX;

	var divDisplayAdjust = document.getElementById("divDisplayAdjust");
	divDisplayAdjust.innerHTML = "";
	divDisplayAdjust.style.top = top + "px";
	divDisplayAdjust.style.left = left + "px";
	
	ajaxLoad('post','pivAdjust.php',data,'divDisplayAdjust');
	divDisplayAdjust.style.display = "block";	
}
	
function submit_adjust(match_id){
	var data = "match_id=" + match_id;
	data+="&match_date=" + document.getElementById("lbMatchDate").innerHTML;
	data+="&pivcode=" + document.getElementById("txtpivcode").value;
		
	data += "&stg1=" + document.getElementById("sltSTG1").value.replace("+","p");
	data += "&stg1A=" + document.getElementById("sltRateSTG1_A").value;
	data += "&stg1B=" + document.getElementById("sltRateSTG1_B").value;
	
	data += "&stg2=" + document.getElementById("sltSTG2").value.replace("+","p");
	data += "&stg2A=" + document.getElementById("sltRateSTG2_A").value;
	data += "&stg2B=" + document.getElementById("sltRateSTG2_B").value;
	
	data += "&stgsumscore=" + document.getElementById("sltSTGsumScore").value.replace("+","p");
	data += "&stgH=" + document.getElementById("sltRateSTGH").value;
	data += "&stgL=" + document.getElementById("sltRateSTGL").value;
	data += "&stgD=" + document.getElementById("sltRateSTGD").value;
	data += "&stgS=" + document.getElementById("sltRateSTGS").value;
	
	document.getElementById("divDisplayAdjust").innerHTML = "<br><center><b>กำลังบันทึก</b>..<br/><img src='../image/loading3.gif' /></center><br/>";
	ajaxLoad('post','pivAdjust_submit.php',data,'divDisplayAdjust');
	
}

/*
function change_passwd(){
	if(document.getElementById("txtOldpasswd").value == ""){
		document.getElementById("txtOldpasswd").focus();
		return false;
	}
	else if(document.getElementById("txtnewpasswd1").value =="" || document.getElementById("txtnewpasswd2").value == ""){
		if(document.getElementById("txtnewpasswd1").value == "") document.getElementById("txtnewpasswd1").focus();
		else  document.getElementById("txtnewpasswd2").focus();
		return false;
	}
	else if(document.getElementById("txtnewpasswd1").value != document.getElementById("txtnewpasswd2").value){
		alert("รหัสใหม่ไม่ตรงกัน !!.");
		document.getElementById("txtnewpasswd2").focus();
		return false;
	}
	
	var pwd_new = document.getElementById("txtnewpasswd2").value;
	if(pwd_new.length < 4){
			alert("รหัสผ่านต้องมีอย่างน้อย 4 ตัว !!.");
			document.getElementById("txtnewpasswd1").value = "";
			document.getElementById("txtnewpasswd2").value = "";
			document.getElementById("txtnewpasswd1").focus();
			return false;
	}
	
	var data =  "pwd_old=" + document.getElementById("txtOldpasswd").value;
	data += "&pwd_new=" + pwd_new;
	data += "&chge_pivcode=" + document.getElementById("txtpivcode").value;
	fmchg_pwd.reset();
	ajaxLoad('post','change_pass_ss.php',data,'divWinchangePWD');

}
*/
-->
</script>
<style type="text/css">
<!--
body{
	font-family:Tahoma;
	font-size:13px;
	margin:3px;
	background-color:#069;
	color:#000080;
	
	}
	
.menu_right_border{
	border-right:solid 2px #FFF;
}

input,textarea,select{
	border:solid 2px #FF7F00;
}

button{
	cursor:pointer;
}

#td_{
	color:#FFF; 
	font-weight:bold; 
	background-image:url(../image/title_bg.png); 
	background-repeat:repeat-x;
}
-->
</style>

</head>

<body>
<?php
require_once("../condb.php");

$pivcode = $_SESSION["piv_adjustCode"];
$uid = $pivcode;
?>
<div style="width:98%; height:auto; border:solid 3px #FFF;  margin:auto; background-color:#AABFFF;">


<div style=" width:100%; height:auto; background-color:#7F9FFF;">

<div style="width:100%; height:10px; background-color:#666; border-bottom:solid 2px #FFFFFF;"></div>

<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr align="center" height="25">
        <td width="265" align="left"></td>
        <td><input type="hidden" id="txtpivcode" value="<?=$pivcode?>" /></td>
        <td>&nbsp;</td>
        <td width="200" valign="baseline"><span style="cursor:pointer;" onclick="window.open('../table/index.php?useMyTable=Y&pivcode=<?=$pivcode?>','','')"><u><img src="../image/print.gif" />พิมพ์ราคาออกเอง</u></span></td>
<!--
       <td valign="baseline" align="right" width="110">
    <span style="color:#03F; cursor:pointer;" onclick="document.getElementById('divWinchangePWD').innerHTML='';  document.getElementById('divWinchangePWD').style.display = 'block'; ajaxLoad('post','change_pass.php','','divWinchangePWD');"><img src="../image/key16.png" />เปลี่ยนรหัส</span>
    
<div id="divWinchangePWD" style="width:260px; height:auto; position:absolute; display:none; z-index:1000; border:solid 8px #FFF; background-color:#EBEBEB;"></div>
       </td>
        
        <td align="right" valign="baseline" width="250">
        <img src="../image/userlogin.png" />Agent Login :&nbsp;<font color="#0000FF"><b><label id="lbuname"><?//$uid?></label></b><br/><?//"(".$_SESSION["adjust_pivname"].")"; ?></font>
        </td>
  
        <td valign="baseline" width="100" align="right"><span style=" color:#D40000; cursor:pointer;" onmouseover="this.style.color='#FFBF00';" onmouseout="this.style.color='#D40000';" onclick="window.location='pivAdjust_logout.php';"><b><img src="../image/logout.png" /> Logout</b></span>&nbsp;&nbsp;</td>
 -->	 
    </tr>
    
</table>

</div>



<?php

$query_matchDate = mysql_query("select match_date from match_h where matchStatus = 'Active';",$conn);
if(mysql_num_rows($query_matchDate) < 1){
	mysql_close($conn);
	echo "<br/><br/><br/><center>ยังไม่มีตาราง Active ในขณะนี้ !!.</center>";
	exit(0);
}else{
	$result_matchDate = mysql_fetch_array($query_matchDate);
	$match_date =$result_matchDate["match_date"];
}

$sql="SELECT m1.match_cancelFlag,
m1.match_detail_id,
m1.match_date,
m1.match_time,
m1.teamOfTheMatch,
m1.team_A_id,
m1.team_A_name,
m1.team_B_id,
m1.team_B_name,
m1.league_id,
m1.league_name,
m1.No_STG1A,
m1.No_STG1B,
m1.No_STG2A,
m1.No_STG2B,
m1.No_STGHigh,
m1.No_STGLow,
m1.No_STGDual,
m1.No_STGSingle,
m1.score_team_A,
m1.score_team_B ,

m2.score_STG1,
m2.rate_STG1_A,
m2.rate_STG1_B,
m2.score_STG2,
m2.rate_STG2_A,
m2.rate_STG2_B,
m2.score_sumSTG,
m2.rate_STGHigh,
m2.rate_STGLow,
m2.rate_STGDual,
m2.rate_STGSingle

FROM match_detail m1 INNER JOIN piv_match_detail m2 ON m1.match_detail_id = m2.match_detail_id 
AND m1.match_date = m2.match_date 
WHERE m1.match_date = '".$match_date ."'  AND m2.piv_code = '".$pivcode."'
ORDER BY m1.league_id, m1.match_detail_id asc; ";
$query = mysql_query($sql,$conn);
 

$arr_date = explode('-',$match_date);
$new_date = $arr_date[2].'-'.$arr_date[1].'-'.$arr_date['0'];

?>

<br/>
<div style="width:1180px; height:auto; margin:auto; background-color:#F5F5F5;">
<br/>
<div style="width:1120px; height:auto; background-color:#FFF; margin:auto; border:solid 4px #FFFFFF;">

<center><font size="+2">ปรับราคา ตารางแข่ง วันที่  <?=$new_date?></font><br/></center>
&nbsp;&nbsp;&nbsp;<font color="#FFFFFF"><label id="lbMatchDate"><?=$match_date?></label></font>&nbsp;
<br/>
<center><font color="#D40000">(มีผลเฉพาะกับโต๊ะ ที่ใช้ราคาปรับเอง เท่านั้น)</font></center>

<table width="100%" cellpadding="2" cellspacing="1" border="0" bgcolor="#AABFFF">
<tr align="center" height="40" style="color:#FFF;" bgcolor="#3366CC">  
	<td><font color="#FFDF00"><b>ปรับ<br/>ราคา</b></font></td>
    <td>เวลา</td>
    <td>ผลแข่ง</td>
    <td colspan="3" align="right">ทีมเจ้าบ้าน&nbsp;&nbsp;</td>
    <td><u>ลูกต่อ1</u></td>
    <td colspan="3" align="left">&nbsp;&nbsp;ทีมเยือน</td>
    <td colspan="2">เจ้าบ้าน</td>
    <td><u>ลูกต่อ2</u></td>
    <td colspan="2">เยือน</td>
    <td colspan="2">สูง</td>
    <td><u>ป.สูงต่ำ</td>
    <td colspan="2">ต่ำ</td>
    <td colspan="2">คู่</td>
    <td colspan="2">คี่</td>
</tr>

<?php
$league = "";

for($i=1; $i<=mysql_num_rows($query); $i++){
$result = mysql_fetch_array($query);
if($league != $result["league_id"]){
	
?>
<tr align="center" bgcolor="#2A3F55">
    <td colspan="24" align="left">&nbsp;&nbsp;<b><font color="#FFFFFF"><?=$result["league_name"]?></font></b></td>

</tr>
<?php
}
	
	$font_colorA = "";
	$font_colorA2 = "";
	$font_colorB = "";
	$font_colorB2 = "";

if($result["teamOfTheMatch"] == "A"){
	$font_colorA = "<font color='red'>*<u><b>";
	$font_colorA2 = "</b></u></font>";
	$font_colorB = "<font color='blue'>";
	$font_colorB2 = "</font>";
	
}else if($result["teamOfTheMatch"] == "B") {
	$font_colorA = "<font color='blue'>";
	$font_colorA2 = "</font>";
	$font_colorB = "<font color='red'>*<u><b>";
	$font_colorB2 = "</b></u></font>";
}

$rid = $id = $result["match_detail_id"];

$score = "";
if($result["match_cancelFlag"] != "Y") $score = "<b>".$result["score_team_A"]."&nbsp;-&nbsp;".$result["score_team_B"]."</b>";
else $score = '<font size="2">ยกเลิก</font>';


//----------------------------------------------------------------START---------------------------------------------------------------------------------------------//
	/// ตรวจดูว่ามีการแข่งขันไปแล้วหรือยัง
	$date1="";
	
	$db_date = $match_date;
	$db_time = $result["match_time"];
	
	$arr_time = explode(":",$db_time);
	$arr_date = explode("-",$db_date);
	$match_time = $arr_time[0].".".$arr_time[1];
	
	if(($match_time >= 12.00) && ($match_time <= 23.59)){
		$date1 = $arr_date[0].$arr_date[1].$arr_date[2];  //set date 1	
		
	}else if(($match_time >= 00.00) && ($match_time <= 11.59)){
		
		$new_date = new DateTime($db_date);
		$date1_d=date_format($new_date,'d');
		$date1_m=date_format($new_date,'m');
		$date1_Y=date_format($new_date,'Y');
		$date1_lastDay=date_format($new_date,'t');
		
		if($date1_d == $date1_lastDay){
			$date1_d="01";
			if($date1_m == 12){
				$date1_m="01";
				$date1_Y=$date1_Y +1;
			}else $date1_m = sprintf("%02d",($date1_m+1));
		}else $date1_d=sprintf("%02d",($date1_d+1));
		
		$date1 = $date1_Y.$date1_m.$date1_d;  //set date 1		
	}//end else;
	
	$matchDateTime = $date1.$arr_time[0].$arr_time[1];
	$dateNow = date("Ymd").date("Hi");
	
//------------------------------------------------------------------END-------------------------------------------------------------------------------------------//
//สีตั้งต้น
$tr_color = "#E7EAFE";
$sty_color_STG = "#00F";
$sty_color_rate = "black";

if($matchDateTime <= $dateNow){
	//แข่งแล้วให้เป็นสีเทา
	$tr_color="#FFFBF0"; 
	$sty_color_STG = "#ACACAC";
	$sty_color_rate = "#ACACAC";
	$font_colorA = "<font color='#ACACAC'>";
	$font_colorA2 = "</font>";
	$font_colorB = "<font color='#ACACAC'>";
	$font_colorB2 = "</font>";
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------//


?>	
<tr align="center" height="26" bgcolor="<?=$tr_color?>" onmouseover="adjustBox_hidden(); this.style.backgroundColor = '#FFFF99';"  onmouseout="this.style.backgroundColor = '';">
    <td width="60" bgcolor="#F5F5F5" align="left"><button type="button" onclick="adjust('<?=$id?>');"><img src='../image/b_edit.png' /></button>&nbsp;<?=$id?></td>

    <td width="42"><?=substr($result["match_time"],0,5)?></td>
    <td width="45" bgcolor="#EAEAEA"><?=$score?></td>
    <td align="right"><?phpecho $font_colorA.$result["team_A_name"].$font_colorA2?>&nbsp;</td>
    <td width="25" style="color:red;"><u><?=$result["No_STG1A"]?></u></td>
    
    <td width="28" id="STG1A<?=$rid?>" onclick="adjustBox_show('STG1A','<?=$rid?>');" onmousemove="adjustBox_show('STG1A','<?=$rid?>');" style="cursor:pointer; color:<?=$sty_color_rate?>;"><?=$result["rate_STG1_A"]?></td>
        <!-- STG1 -->
    <td width="42" style="color:<?=$sty_color_STG ?>; cursor:pointer;"  bgcolor="#FFFFCC" onclick="adjustBox_show('STG1','<?=$rid?>');" onmousemove="adjustBox_show('STG1','<?=$rid?>');" ><b><span id="STG1<?=$rid?>"><?=$result["score_STG1"]?></span></b></td>
    <!-- /// -->
    <td width="28" id="STG1B<?=$rid?>" onclick="adjustBox_show('STG1B','<?=$rid?>');" onmousemove="adjustBox_show('STG1B','<?=$rid?>');"  style="cursor:pointer; color:<?=$sty_color_rate?>;"><?=$result["rate_STG1_B"]?></td>
    
    <td width="25" style="color:red;"><u><?=$result["No_STG1B"]?></u></td>
    <td align="left">&nbsp;<?phpecho $font_colorB.$result["team_B_name"].$font_colorB2?></td>
   
    <td style="color:red;"><u><?=$result["No_STG2A"]?></u></td>
    
    <!-- //////  -->
    <td width="28" id="STG2A<?=$rid?>" onclick="adjustBox_show('STG2A','<?=$rid?>');" onmousemove="adjustBox_show('STG2A','<?=$rid?>');" style="cursor:pointer; color:<?=$sty_color_rate?>;"><?=$result["rate_STG2_A"]?></td>
    <!-- STG2 -->
    <td width="42" style="color:<?=$sty_color_STG ?>; cursor:pointer;"  bgcolor="#CCFFFF"  onclick="adjustBox_show('STG2','<?=$rid?>');" onmousemove="adjustBox_show('STG2','<?=$rid?>');"><b><span id="STG2<?=$rid?>"><?=$result["score_STG2"]?></span></b></td>
    <!-- //////  -->
    <td width="28" id="STG2B<?=$rid?>" onclick="adjustBox_show('STG2B','<?=$rid?>');" onmousemove="adjustBox_show('STG2B','<?=$rid?>');" style="cursor:pointer; color:<?=$sty_color_rate?>;"><?=$result["rate_STG2_B"]?></td>
    <!-- //////  -->
    <td style="color:red;"><u><?=$result["No_STG2B"]?></u></td>
    
    
    <td style="color:red;"><u><?=$result["No_STGHigh"]?></u></td>
        
    <td width="28" id="STGH<?=$rid?>" onclick="adjustBox_show('STGH','<?=$rid?>');" onmousemove="adjustBox_show('STGH','<?=$rid?>');" style="cursor:pointer; color:<?=$sty_color_rate?>;"><?=$result["rate_STGHigh"]?></td>
    
    <td width="50" style="color:<?=$sty_color_STG ?>; cursor:pointer;"  bgcolor="#FFCCFF"  onclick="adjustBox_show('STGHL','<?=$rid?>');" onmousemove="adjustBox_show('STGHL','<?=$rid?>');"><b><span id="STGHL<?=$rid?>"><?=$result["score_sumSTG"]?></span></b></td>
    
    <td width="28" id="STGL<?=$rid?>" onclick="adjustBox_show('STGL','<?=$rid?>');" onmousemove="adjustBox_show('STGL','<?=$rid?>');" style="cursor:pointer; color:<?=$sty_color_rate?>;"><?=$result["rate_STGLow"]?></td>
    
    <td style="color:red;"><u><?=$result["No_STGLow"]?></u></td>
    
   
    <td style="color:red;"><u><?=$result["No_STGDual"]?></u></td>
    
    <td width="28" id="STGD<?=$rid?>" onclick="adjustBox_show('STGD','<?=$rid?>');" onmousemove="adjustBox_show('STGD','<?=$rid?>');" style="cursor:pointer; color:<?=$sty_color_rate?>;"><?=$result["rate_STGDual"]?></td>
    <td width="28" id="STGS<?=$rid?>" onclick="adjustBox_show('STGS','<?=$rid?>');" onmousemove="adjustBox_show('STGS','<?=$rid?>');" style="cursor:pointer; color:<?=$sty_color_rate?>;"><?=$result["rate_STGSingle"]?></td>
    
    <td style="color:red;"><u><?=$result["No_STGSingle"]?></u></td>
</tr>
<?php
$league = $result["league_id"];

}

mysql_close($conn);
?>
</table>

    </div>
    <br/><br/>
    
</div>
<br/>

</div>

<br/><br/>


<div id="divDisplayAdjust" style="width:350px; height:auto; border:solid 5px #000; position:absolute; display:none; background-color:#AABFFF; z-index:1000;"></div>

<div id="divAdjustBox" style="width:140px; height:auto; position:absolute; display:none; z-index:1000; background:#00C; border:solid 3px  #000000;" ></div>


</body>
</html>