<?php require_once("header-text-html.php");
	@session_start();
	if(!isset($_SESSION["sysadmin"])) echo '<script type="text/javascript">window.location="login_admin.php";</script>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin</title>
<script type="text/javascript">
<!--
function menu_OnmouseOver(el){
	el.style.backgroundColor = '#FFFF66';
}
function menu_OnmouseOut(el){
	el.style.backgroundColor = '';
}

function open_page(url){
	url += "?dummy="+(new Date()).getTime();
	window.open(url,'ifrmAdmin','');

}

-->
</script>
<style type="text/css">
<!--
body{
	font-family:Tahoma;
	font-size:12px;
	margin:4px;
	background-color:#036;
	
	}
.menu_right_border{
	border-right:solid 1px #036;
	cursor:pointer;
}
-->
</style>
</head>

<body>
<br/>
<div style="width:99%; height:auto; margin:auto;">

<div style="width: 100%; height: auto; background-color:#AABFFF; font-size:14px; color:#000080;">

<table width="100%" cellpadding="1" cellspacing="0" border="0">
    <tr height="40" align="center">
        <td width="180" align="left" bgcolor="#003366">&nbsp;&nbsp; <img src="image/membersM.png" /><font color="#FFFF00"><label id="lbuname"><?=$_SESSION["sysadmin"]?></label></font></td>
               
        <td width="135" class="menu_right_border" onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);" onclick="open_page('matchManagement.php');"><img src="image/ball24.png" />ตารางแข่ง</td>
        
        <td width="135" class="menu_right_border" onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);" onclick="open_page('dataMaster.php');"><img src="image/leage.png" />ข้อมูลทีม,ลีกส์</td> 
        
        <?php
		if($_SESSION["sysadmin"] == "admin" || $_SESSION["sysadmin"] == "admin2" ){
		?>
        <td width="135" class="menu_right_border" onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);" onclick="open_page('userManagement.php');"><img src="image/user.png" />ข้อมูลผู้ใช้งาน</td>
         
        <td width="135" class="menu_right_border" style="cursor:pointer;" onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);" onclick="open_page('reports/statistic_index_rpt.php');"><img src="image/chart-icon.png" />สถิติการแทง </td><!--  สรุปยอดขาย   การรับจ่ายเงิน  -->

        <td width="140" style="cursor:pointer;" onmouseover="menu_OnmouseOver(this);" onmouseout="menu_OnmouseOut(this);" onclick="open_page('admin_index_report.php');"><img src="image/ball24-6.png" />รายงานภาพรวม </td><!--  สรุปยอดขาย   การรับจ่ายเงิน  -->

        <td width="160" bgcolor="#003366" onmouseover="this.style.color = '#FFFF55';"  onmouseout="this.style.color='#AADFFF'; " style="color:#AADFFF;"><span style="cursor:pointer;"  onclick="open_page('admin_change_pwd.php');"><img src="image/key24.png" />เปลี่ยนรหัส(Admin)</span></td>
       
        <?php
		}
		?>
    <td bgcolor="#003366">&nbsp;</td>
    <td  width="100" align="right" bgcolor="#003366"><span style=" color:#F05; cursor:pointer;" onmouseover="this.style.color='#FFFF00';" onmouseout="this.style.color='#F05';" onclick="window.location='logout_admin_ss.php';"><img src="image/unlock.png" /> <u>Logout</u></span>&nbsp;&nbsp;</td>

    </tr>
</table>

</div>


<iframe id="ifrmAdmin" name="ifrmAdmin"  width="100%" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto"></iframe>



</div>

<div id="divMainLoading" style=" width:220px; height:auto; background-color:#F5F5F5; border:solid 8px #7F0000; position:absolute; display:none; top:270px; text-align:center; z-index:2000;">
	<h3><img src="image/loading3.gif" /><br/><span id="spTextLoad"></span></h3>
</div>

</body>
</html>

<script type="text/javascript">
<!--
document.getElementById("ifrmAdmin").style.height  = (screen.availHeight - 120)+ "px";
open_page('matchManagement.php');
-->
</script>