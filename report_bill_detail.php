<?php require_once("header-text-html.php") ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>รายงานแบบละเอียด (บอลเต็ง) (บอลสเต็ป)</title>
<style type="text/css">
<!--
body{
	font-family:Tahoma;
	font-size:13px;
	margin:3px;
	background-color:#036;
	color:#000080;
	
	}

-->
</style>
</head>

<body>
<?php
require_once("condb.php");
$dt = $_GET["dt"];
$bnc_id = $_GET["bnc_id"];
$bnc_name = $_GET["bnc_name"];
$user_id = $_GET["user_id"];


//-----------------------ตรวจสอบสถานะการบันทึกผล การแข่งขัน ของว่าตารางว่า ครบทุกคู่หรือยัง------------------------------------------------------------//
$query_chk_table_status = mysql_query("select * from match_h where match_date ='".$dt."' and matchStatus !='Draft'  order by  match_date desc ;",$conn);

if(mysql_num_rows($query_chk_table_status) > 0){
	$result_chk_table_status = mysql_fetch_array($query_chk_table_status);
	if($result_chk_table_status["matchStatus"] == "Saved") $alert_table_status = '<font color="#007F00">'."การบันทึกผลสมบูรณ์แล้ว".'</font>';
	else $alert_table_status ='<font color="#D40000">'. "ยังไม่สมบูรณ์ (ตารางวันที่  " . $result_chk_table_status['match_date'] . " ยังบันทึกผลไม่ครบทุกคู่ !!.)".'</font>';

}else{	
	$alert_table_status ='<font color="#D40000">'. "ยังไม่สมบูรณ์ (ตารางวันที่  " .$dt." ยังไม่บันทึกผลแข่ง !!.)".'</font>';	
}
 
//-------------------------------------------------------------------------------------##----------------------------------------------------------------///

?>
<div style="width:98%; height:auto; border:solid 3px #FFF;  margin:auto; background-color:#AABFFF;">
<div style=" width:100%; height:auto;">

<br/>
<div style="width:1100px; height:auto; margin:auto; background-color:#FFF;">
    <font size="3">
    &nbsp;ตารางแข่งขัน วันที่ <font color="#0000FF"><?=$dt?></font>&nbsp;&nbsp; สาขา <font color="#0000FF"><?=$bnc_name?></font> &nbsp;&nbsp; Username : <font color="#0000FF"><?=$user_id?></font>
    </font>

<br/>
<?php
$arr_bill_type = array('one','step');
$string_type = array('บอลเต็ง', 'บอลสเต็ป');
$match_status = array('N'=>'จบ', 'Y'=>'ยกเลิกแข่ง');
for($LoopType =0; $LoopType < 2; $LoopType ++){//for(0) select 2 ครั้ง บอลเต็ง บอลสเต็ป

$money_in = 0;
$money_prepay = 0;
$bill_N = 0;

?>
<br/><br/>
<center>
<h2><font color="#000000">รายงานบิลแบบละเอียด (<?=$string_type[$LoopType]?>)</font></h2>
<font size="3"><b>สถานะการบันทึกผล -- <?=$alert_table_status?></b></font>
</center>

<table align="center" width="1000" cellpadding="2" cellspacing="1" border="0" bgcolor="#EBEBEB">
<tr align="center" height="35" style="color:#FFF; font-size:14px;" bgcolor="#003366">  
	<td>บิลเลขที่</td>
    <td>รหัสคู่</td>
    <td>เวลาแข่ง</td>
    <td>แทง</td>
    <td>สถานะทีม</td>
    <td>ชื่อทีมที่แทง</td>
    <td>หน้าบอล</td>
    <td>ราคา</td>
    <td>ผลบอล</td>
    <td colspan="2">ผลแทง</td>
    <td>เงินได้</td>
    <td>เงินได้ทั้งหมด</td>
</tr>
<?php

$sql = "SELECT bill_h.bill_id, bill_h.bill_date, bill_h.bill_time, bill_h.costs, bill_h.pay_amount,
bill_line.*, 
match_detail.match_cancelFlag, match_detail.match_time, match_detail.teamOfTheMatch, match_detail.team_A_name, match_detail.team_B_name,
match_detail.score_team_A, match_detail.score_team_B
FROM bill_h 
INNER JOIN bill_line ON bill_h.bill_id = bill_line.bill_id
INNER JOIN match_detail ON bill_line.match_detail_id = match_detail.match_detail_id
WHERE bill_h.match_date = '".$dt."'
	AND match_detail.match_date = '".$dt."'
	AND bill_h.bnc_id = '".$bnc_id."'
	AND bill_h.create_by_id = '".$user_id."'
	AND bill_h.cancel_flag = 'N'
	AND bill_h.bill_type = '".$arr_bill_type[$LoopType]."'
ORDER BY  bill_h.bill_number ASC ;
";

$query = mysql_query($sql,$conn);

$text_status = array("A"=>"เจ้าบ้าน","B"=>"เยือน");

for($i=1; $i<= mysql_num_rows($query); $i++){ //for(1)
	$result = mysql_fetch_array($query);
	
							//รูปภาพ
	//--------------------------------------------------------------------------------//
	$stage_status = explode(",",$result["bill_line_paymentTerm"]);
	switch($stage_status[0]){
		case "0": $img_stage1='<img src="image/false.png" width="15" height="15" />' ; break; //ตาย
		case "1": $img_stage1='<img src="image/o.png" width="15" height="15" />' ; break; //เสมอ
		case "2": $img_stage1='<img src="image/true.png" width="15" height="15" />' ; break; //ชนะ
		default :  $img_stage1 = "";
	}
	switch($stage_status[1]){
		case "0": $img_stage2='<img src="image/false.png" width="15" height="15" />' ; break; //ตาย
		case "1": $img_stage2='<img src="image/o.png" width="15" height="15" />' ; break; //เสมอ
		case "2": $img_stage2='<img src="image/true.png" width="15" height="15" />' ; break; //ชนะ
		default :  $img_stage2 = "";	
	}
	//--------------------------------------------------------------------------------//

	$AB_HL_DS = $result["teamOfTheMatch"];
	$gold = $result["bill_line_sub_stageIndex"];	
		
		if($gold == "A" || $gold == "B"){
			
			$team_status = $text_status["$gold"];
			$team_display = $result["team_".$gold."_name"];
			if($AB_HL_DS == "-") $txt_state = "-";
			else if($gold == $AB_HL_DS){
				$txt_state = "ต่อ";
				$color = "#D40000";
			}
			else{
				$txt_state = "รอง";
				$color = "#0000FF";
			}
		}else{//1
		    $team_display = $result["team_A_name"];	
			$team_status = $text_status["A"];
			switch($gold){
				case "H": $txt_state = "สูง" ; break;
				case "L": $txt_state = "ต่ำ" ; break;
				case "D": $txt_state = "คู่" ; break;
				case "S": $txt_state = "คี่" ; break;			
			}//end switch();
		}//end else 1;

if( $chk_bill_id != $result["bill_id"]){
	$bill_N +=1;
	bill_header();
}
?>

<tr align="center" bgcolor="#FFFFFF" onmousemove="this.style.backgroundColor = '#FFFF99';" onmouseout="this.style.backgroundColor = '';">
    <td width="100" align="left"><font color="#555F55"><?phpif($result["score_team_A"] != "" || $result["match_cancelFlag"] == "Y") echo $match_status[$result["match_cancelFlag"]];?></font></td>
    <td width="45">R<?=$result["match_detail_id"]?></td>
    <td width="60"><?=substr($result["match_time"],0,5)?></td>
    <td width="45">(<b><font color="<?=$color?>"><?=$txt_state?></font></b>)</td>
    <td width="70"><?=$team_status?></td>
    <td align="left">&nbsp;(<b><?=$result["number"]?></b>)<font color="<?=$color?>"><?=$team_display?></font></td>
    <td width="70"><?=$result["STG_score"]?></td>
    <td width="50"><?=$result["STG_rate"]?></td>
    <td width="60"><b><?=$result["score_team_A"]?>&nbsp;-&nbsp;<?=$result["score_team_B"]?></b></td>
    <td width="25"><?=$img_stage1?></td>
    <td width="25"><?=$img_stage2?></td>
    <td width="70"><?=$result["bill_line_pay"]?></td>
    <td width="100">&nbsp;</td>
</tr>
<?php

$chk_bill_id = $result["bill_id"];
}//end for(1);

?>

</table>
<br/>
<div style="width:977px; height:50px; margin:auto; text-align:center; font-size:16px; background-color:#D4FFFF; font-weight:bold;">
<br/>
สรุปบิล (<?=$string_type[$LoopType]?>)&nbsp; &nbsp;&nbsp;&nbsp;
จำนวนบิล <?=number_format($bill_N)?> ใบ&nbsp; &nbsp;&nbsp;&nbsp;<font color="#0000FF">เงินแทงเข้า : <?=number_format($money_in)?></font>&nbsp; &nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<font color="#D40000">เตรียมจ่าย : <?=number_format($money_prepay)?></font>
</div>

<?php
}//end for(0)//

function  bill_header(){
global $result;
global $money_in;
global $money_prepay;

$money_in += $result["costs"];
$money_prepay += $result["pay_amount"];

if($result["pay_amount"] == 0){
	$pay_color = "#D40000";
	$pay_money = "-".$result["costs"] ;
}
else {
	$pay_color = "#007F00";
	$pay_money = $result["pay_amount"] ;
}
?>	
<tr height="5" style="color:#FFF; background-image:url(image/title_bg.png); background-repeat:repeat-x;">
	<td colspan="13"></td>
</tr>
<tr height="25">
	<td colspan="12"><b><?=$result["bill_id"];?></b>&nbsp;&nbsp;(<font color="#0000FF"><b>เงินแทง&nbsp;<?=number_format($result["costs"]);?></b></font>)&nbsp;&nbsp;<?=$result["bill_date"]." ".substr($result["bill_time"],0,5);?></td>
	<td align="right"><b><font color="<?=$pay_color?>" size="3"><?=number_format($pay_money); ?></font></b>&nbsp;</td>
</tr>
<?php}?>

<br/>
</div>
<br/><br/>
</div>

<?php
mysql_close($conn);
?>

</body>
</html>