<?php header("Content-Type:text/html; charset=utf-8");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script type="text/javascript" src="ajax_framework.js"></script>
<script type="text/javascript">
<!--
function display(url,display_id){	
	ajaxLoad('post',url,'',display_id);
}

function shw_data(st,pvcode,name,tel,addr){

	document.getElementById("chkbox").checked = false;
	hidden_txt();
	
	document.getElementsByName("txtchk").item(0).value = "UPDATE";	
	document.getElementsByName("txtPVcode").item(0).value = pvcode;
	document.getElementsByName("txtName").item(0).value = name;
	document.getElementsByName("txtTel").item(0).value = tel;
	document.getElementsByName("txtAddr").item(0).value = addr;
	document.getElementById("rdoctmStatus" + st).checked = true;

	
}

function hidden_txt(){
	
	if(document.getElementById("chkbox").checked == true){
	document.getElementsByName("txtPVcode").item(0).disabled = "";
	document.getElementsByName("txtName").item(0).disabled = "disabled";
	document.getElementsByName("txtTel").item(0).disabled = "disabled";
	document.getElementsByName("txtAddr").item(0).disabled = "disabled";
	document.getElementById("btSave").hidden = true;
	document.getElementById("btReset").hidden = true;
	fm.reset();
	document.getElementById("chkbox").checked = true;
	document.getElementsByName("txtPVcode").item(0).focus();
	}else{
		clear_txt();
		fm.reset();
	}
}


function clear_txt(){
	document.getElementsByName("txtPVcode").item(0).disabled = "disabled";
	document.getElementsByName("txtName").item(0).disabled = "";
	document.getElementsByName("txtTel").item(0).disabled = "";
	document.getElementsByName("txtAddr").item(0).disabled = "";
	document.getElementById("btSave").hidden = false;
	document.getElementById("btReset").hidden = false;

}

function ctm_save(){

	if(document.getElementsByName("txtName").item(0).value == ""){
		alert("กรุณาระบุ ชื่อลูกค้า ก่อน !!.");
		document.getElementsByName("txtName").item(0).focus();
		return false;
	}

	if(document.getElementsByName("txtchk").item(0).value == "UPDATE"){
		if(confirm("ต้องการแก้ไขรายละเอียด Agent ID  " + document.getElementsByName("txtPVcode").item(0).value + " ใช่ หรือ ไม่ ?.")){
			document.getElementsByName("txtPVcode").item(0).disabled = "";
			fm.submit();
			fm.reset();
			document.getElementsByName("txtPVcode").item(0).disabled = "disabled";
		}
		
		return;

	}else if(document.getElementsByName("txtchk").item(0).value == "RESET"){
		
		if(document.getElementsByName("txtPVcode").item(0).value == ""){
			alert("กรุณา คลิกเลือกรายการ ด้านขวามือ ก่อน  !!.");
			return false;
		}

		if(confirm("คุณกำลังจะ Reset รายการ PIV CODE " + document.getElementsByName("txtPVcode").item(0).value + " หลังจากนี้รหัสผ่านคือ "+ document.getElementsByName("txtPVcode").item(0).value + " ยืนยัน ใช่ หรือ ไม่ ?.")){
			document.getElementsByName("txtPVcode").item(0).disabled = "";
			fm.submit();
			fm.reset();
			document.getElementsByName("txtPVcode").item(0).disabled = "disabled";
		}
		return;
	
	}else if(document.getElementsByName("txtchk").item(0).value == "INSERT"){
		fm.submit();
		fm.reset();
		
	}
		
}

function reset_ctm(){	
	document.getElementsByName("txtchk").item(0).value = "RESET";
	ctm_save();
}

function ctm_find(){
	var data ="txtlike=" +  document.getElementsByName("txtPVcode").item(0).value;
	window.open('ctm_query.php?' + data ,'ifrmCustomer','');

}


//------------------------------------ branch page --------------------------------------//
function chk_num(){
	ek=event.keyCode;
	if(ek <48 ||ek >57){
		event.returnValue=false;
	}
}

function  OnPIV_Enter(){
	var data = "pivcode=" + document.getElementById("txtPIVCODE").value;
	ajaxLoad('post','branch_auto_customer.php',data,'displaypivcode');
	document.getElementById("txtpivid").value = "";
}

function get_ctmInfo(pivcode){
	fmbnc.reset();
	document.getElementById("txtpivid").value = pivcode;
	document.getElementById("txtPIVCODE").value = pivcode;
	document.getElementById("lbowner").innerHTML = document.getElementById("tdctmname" + pivcode).innerHTML;
	document.getElementById("displaypivcode").innerHTML = "";
	
	var data = "pivcode=" + pivcode;
	ajaxLoad('post','branch_query.php',data,'displayBranch');
	
}

function submit_branch(){

	if(document.getElementById('txtchk').value == "INSERT"){
		if(document.getElementById("txtpivid").value != document.getElementById("txtPIVCODE").value){
				alert("กรุณาระบุเจ้าของโต๊ะให้ถูกต้อง !!.");
				document.getElementById("txtPIVCODE").focus();
				return false;		
			}
	}else if(document.getElementById('txtchk').value == "UPDATE"){
		if(confirm("ต้องการแก้ไขรายการ ใช่ หรือ ไม่ ?."));
		else {
			document.getElementById('txtbncname').focus();
			return false;
		}
	}

		if(document.getElementById("txtpivid").value == ""){
			alert("กรุณาระบุเจ้าของโต๊ะ !!.");
			document.getElementById("txtPIVCODE").focus();
			return false;
		}	
		
		if(document.getElementById('txtbncname').value == ""){
				alert("กรุณาระบุชื่อโต๊ะก่อน !!.");
				document.getElementById('txtbncname').focus();
				return false;
		}
		
		if(document.getElementById('txtone_max').value == ""){
				alert("กรุณาระบุเงินรับแทงสูงสุดของ บอลเต็ง !!.");
				document.getElementById('txtone_max').focus();
				return false;
		}
		
		if(document.getElementById('txtstep_min').value == "" || document.getElementById('txtstep_min').value  < 2){
				alert("สเต็ป MIN อย่างน้อยต้อง 2 คู่ !!.");
				document.getElementById('txtstep_min').focus();
				return false;
		}
		
		var chk_step_min = parseInt(document.getElementById('txtstep_min').value);
		if(document.getElementById('txtstep_to').value < chk_step_min){
				alert("สเต็ป MAX ต้องไม่น้อยกว่า สเต็ป MIN !!.");
				document.getElementById('txtstep_to').focus();
				return false;
		}
		
		if(document.getElementById('txtstep_max').value == ""){
				alert("กรุณาระบุเงินรับแทงสูงสุดของ บอลสเต็ป !!.");
				document.getElementById('txtstep_max').focus();
				return false;
		}
		
		var data = "id=" + document.getElementById("txtpivid").value;
		data+="&pivcode=" + document.getElementById('txtPIVCODE').value;
		data+="&bncname=" + document.getElementById('txtbncname').value;
		data+="&bncaddr=" + document.getElementById('txtbncAddr').value;
		
		data+="&useMyPrice=" + document.getElementById('sltsetType').value;
		data+="&one_max=" + document.getElementById('txtone_max').value;
		data+="&step_min=" + document.getElementById('txtstep_min').value + "/" + document.getElementById('txtstep_to').value;
		data+="&step_max=" + document.getElementById('txtstep_max').value;
		data+="&stepUpRate=" + document.getElementById("sltStepUpRate").value;
		
		var bncstatus = "Y";
		if(document.getElementById('rdobncStatusY').checked == false) bncstatus = "N";		
		data+="&bncstatus=" + bncstatus;
		
		data+="&bnctxtchk=" + document.getElementById('txtchk').value;
		ajaxLoad('post','branch_insert.php',data,'displayBranch');
		fmbnc.reset();
		document.getElementById("lbowner").innerHTML = "";
		document.getElementById("txtpivid").value = "";
		document.getElementById("txtPIVCODE").disabled = "";

}

function bnc_setDateEdit(i,st,pivcode,bncid,useMyPrice,one_MAX,step_MIN,step_To,step_MAX,stepUpRate){
	document.getElementById('txtchk').value = "UPDATE";
	document.getElementById("txtpivid").value = bncid;
	document.getElementById("txtPIVCODE").value = pivcode;
	document.getElementById('txtbncname').value = document.getElementById("bncname"+i).innerHTML;
	document.getElementById('txtbncAddr').value = document.getElementById("bncaddr"+i).innerHTML;
	document.getElementById("lbowner").innerHTML = document.getElementById("ctmname"+i).innerHTML
	
	document.getElementById("txtone_max").value = one_MAX;
	document.getElementById("txtstep_min").value = step_MIN;
	document.getElementById("txtstep_to").value = step_To;
	document.getElementById("txtstep_max").value = step_MAX;
	
	//+เงินเพิ่ม ตามการตั้งค่า.
	var selectted_stepUpRate = document.getElementById("sltStepUpRate");
	for(var i =0; i< selectted_stepUpRate.options.length; i++) {
		if(selectted_stepUpRate.options[i].value == stepUpRate ) selectted_stepUpRate.selectedIndex = i;
	}
	//----------------------------------------------------------------
	
	var set_useMyPriceIndex =0 ;
	if(useMyPrice == "Y") set_useMyPriceIndex =1;
	document.getElementById('sltsetType').selectedIndex = set_useMyPriceIndex;	
	
	document.getElementById("rdobncStatus" + st).checked = true;
	
	document.getElementById("txtPIVCODE").disabled = "disabled";
}
//------------------------------------end  branch page --------------------------------------//

//------------------------------------userinfo page --------------------------------------//
function loadOption_bnc(pivcode){
	
	var data='pivcode=' + pivcode;
	ajaxLoad('post','userLoad_bnc.php',data,'tdLoadbnc');	
	ajaxLoad('post','user_display.php',data,'displayUser');	

}

function user_display(){
	
	var data='pivcode=' + document.getElementById("txtPIVCODE").value;
	data+="&bncid=" + document.getElementById("sltbnc").value;
	ajaxLoad('post','user_display.php',data,'displayUser');	
}

function fmuser_clr(){
	document.getElementById("tdLoadbnc").innerHTML = '<select id="sltbnc"><option value="-">-</option></select>';
	fmuser.reset();
}

function submit_fmuser(){
	
 if(document.getElementById("txtchk").value == "INSERT"){
	 
	if(document.getElementById("txtPIVCODE").value == ""){
		alert("กรุณาระบุ Agent ID !!.");
		document.getElementById("txtPIVCODE").focus();
		return false;
	}
	
	 if(document.getElementById("sltbnc").value == "-"){
		 alert("กรุณาระบุโต๊ะที่จะใช้งาน !!.");
		 document.getElementById("sltbnc").focus();
		 return false;
		 }
	 
	if(confirm("คุณต้องการเพิ่มชื่อผู้ใช้หน้าโต๊ะ ใช่ หรือ ไม่ ?."));
	else return false;
 }		
		
	 var data = "bncid=" + document.getElementById("sltbnc").value;	
	 data += "&pivcode=" + document.getElementById("txtPIVCODE").value;
	 data += "&txtchk=" + document.getElementById("txtchk").value;
	 data+= "&uid=" + document.getElementById("txtid").value;
	document.getElementById("txtid").value = "";
	document.getElementById("txtchk").value = "INSERT";

	 ajaxLoad('post','user_submit.php',data,'displayUser');
 
}

function user_activate(uid){
	if(document.getElementById("txtPIVCODE").value == ""){
		alert("กรุณาระบุ Agent ID !!.");
		document.getElementById("txtPIVCODE").focus();
		return false;
	}

	 if(confirm("คุณต้องการให้เริ่มใช้งาน  " + uid + " ใช่ หรือ ไม่ ?.")){
		document.getElementById("txtchk").value = "UPDATE";
		document.getElementById("txtid").value = uid;
		submit_fmuser();
	}
}

function user_delete(uid){
	if(document.getElementById("txtPIVCODE").value == ""){
		alert("กรุณาระบุ Agent ID !!.");
		document.getElementById("txtPIVCODE").focus();
		return false;
	}

	 if(confirm("คุณต้องการลบชื่อผู้ใช้ " + uid + " ใช่ หรือ ไม่ ?.")){
		document.getElementById("txtchk").value = "DELETE";
		document.getElementById("txtid").value = uid;
		submit_fmuser();
	}

}
	



//------------------------------------end userinfo page --------------------------------------//

//return false submit form when enter on text box.;
document.onkeydown = chkEvent 
function chkEvent(e) {
	var keycode;
	if (window.event) keycode = window.event.keyCode;// for IE 
	else if (e) keycode = e.which; //for Firefox
	if(keycode==13)
	{
		return false;
	}
}

-->
</script>
<style type="text/css">
<!--
body{
	font-family:Tahoma;
	font-size:13px;
	margin:0px;
	color:#000080;
	}

input,textarea,select{
	border:solid 1px #2A3F55;
	}
	
.bg_img{
	 background-image:url(image/title_bg2.png);
	}

-->
</style>

</head>

<body>

<table id="tbMaster" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#FAFAFA">
<tr>
    <td width="220" valign="top" bgcolor="#7F9FFF">
    <br/>
    	<div style="width:97%; height:auto; margin:auto; font-size:14px;">
        <table width="96%" align="center" cellpadding="5" cellspacing="2" border="0" style="cursor:pointer;" bgcolor="#D4DFFF">
            <tr height="35"><td onmouseover="this.style.color = '#D43F00' ;" onmouseout="this.style.color = '#000080' ;" style="border-bottom:dotted 1px #A1A1A1;" onclick="display('customer.php','divUserMasterDisplay');">&nbsp;รายชื่อลูกค้า (Agent)</td></tr>
            <tr height="35"><td onmouseover="this.style.color = '#D43F00' ;" onmouseout="this.style.color = '#000080' ;" style="border-bottom:dotted 1px #A1A1A1;" onclick="display('branch.php','divUserMasterDisplay');">&nbsp;โต๊ะผู้ใช้บริการ (Branch)</td></tr>
            <tr height="35"><td onmouseover="this.style.color = '#D43F00' ;" onmouseout="this.style.color = '#000080' ;" style="border-bottom:dotted 1px #A1A1A1;" onclick="display('user_info.php','divUserMasterDisplay');">&nbsp;ผู้ใช้หน้าโต๊ะ (User)</td></tr>
        </table>
        <br/>
        </div>
        
    </td>
    <td valign="top" bgcolor="#AABFFF"><div id="divUserMasterDisplay" style="width:100%; height:auto;"></div></td>
</tr>
</table>
</body>
</html>
<script type="text/javascript">
<!--
	document.getElementById("tbMaster").style.height = (screen.availHeight - 150) + "px";
	display('customer.php','divUserMasterDisplay');
-->
</script>