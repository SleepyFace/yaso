<?phprequire_once("header-text-html.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>

<body>
<?php
require_once("condb.php");
$dt1 = $_POST["dt1"];
$dt2 = $_POST["dt2"];
$pivcode = $_POST["pivcode"];

///------เงินเข้า------------------------------------------------
$sql="SELECT branch.bnc_no, count(*) as amount, sum(bill_h.costs) as sum_cost
FROM  bill_h INNER JOIN branch on bill_h.bnc_id = branch.bnc_id
where bill_h.match_date between '".$dt1."' and '".$dt2."'
and branch.ctm_pvid = '".$pivcode."'
and bill_h.cancel_flag = 'N'
GROUP BY bill_h.bnc_id, branch.bnc_no  order by branch.bnc_no asc ;";
$query=mysql_query($sql,$conn);

$arr_money_IN = array();
for($i=1; $i<= mysql_num_rows($query); $i++){
	$result=mysql_fetch_array($query);
	$arr_money_IN[$result["bnc_no"]] = array($result["amount"], $result["sum_cost"]);
}

///------เงินออก------------------------------------------------
$sql="SELECT branch.bnc_no, count(*) as amount, sum(bill_h.pay_amountAccept) as sum_pay_amount
FROM  bill_h INNER JOIN branch on bill_h.bnc_id = branch.bnc_id
where bill_h.payment_date between '".$dt1."' and '".$dt2."'
and branch.ctm_pvid = '".$pivcode."'
and bill_h.pay_amountAccept > 0
and bill_h.cancel_flag = 'N'
GROUP BY bill_h.bnc_id, branch.bnc_no  order by branch.bnc_no asc ;";
$query=mysql_query($sql,$conn);

$arr_money_OUT = array();
for($i=1; $i<= mysql_num_rows($query); $i++){
	$result=mysql_fetch_array($query);
	$arr_money_OUT[$result["bnc_no"]] = array($result["amount"], $result["sum_pay_amount"]);
}

///------เงินเตรียมจ่าย------------------------------------------------
$sql="SELECT branch.bnc_no, count(*) as amount, sum(bill_h.pay_amount) as pay_amount
FROM  bill_h INNER JOIN branch on bill_h.bnc_id = branch.bnc_id
where bill_h.match_date between '".$dt1."' and '".$dt2."'
and branch.ctm_pvid = '".$pivcode."'
and bill_h.pay_amount > 0
and bill_h.cancel_flag = 'N'
GROUP BY bill_h.bnc_id, branch.bnc_no  order by branch.bnc_no asc ;";
$query=mysql_query($sql,$conn);

$arr_moneyPay = array();
for($i=1; $i<= mysql_num_rows($query); $i++){
	$result=mysql_fetch_array($query);
	$arr_moneyPay[$result["bnc_no"]] = array($result["amount"], $result["pay_amount"]);
}

///------บิลยกเลิก-----------------------------------------------
$sql="SELECT branch.bnc_no, count(*) as amount, sum(bill_h.costs) as sum_cost_cancel
FROM  bill_h INNER JOIN branch on bill_h.bnc_id = branch.bnc_id
where bill_h.match_date between '".$dt1."' and '".$dt2."'
and branch.ctm_pvid = '".$pivcode."'
and bill_h.cancel_flag = 'Y'
GROUP BY bill_h.bnc_id, branch.bnc_no  order by branch.bnc_no asc ;";
$query=mysql_query($sql,$conn);

$arr_Bill_Cancel = array();
for($i=1; $i<= mysql_num_rows($query); $i++){
	$result=mysql_fetch_array($query);
	$arr_Bill_Cancel[$result["bnc_no"]] = array($result["amount"], $result["sum_cost_cancel"]);
}
?>
<br/>
<center><font size="4"><B>สรุปรายงาน รายรับ - รายจ่าย ของแต่ละสาขา</B></font></center>
<br/>
<table width="90%" align="center" cellpadding="0" cellspacing="0" border="1" bordercolor="#E0E0E0" bgcolor="#FFFFFF"  style="font-size:16px; font-weight:bold;"> 
    <tr height="30" align="center" style="background-image:url(image/title_bg2.png);">
        <th>รายชื่อสาขา</th>
        <th width="150">แสดงรายการ</th>
        <th>จำนวนบิล(ใบ)</th>
        <th>จำนวนเงิน(บาท)</th>
    </tr>
<?php
$sql="select * from branch where ctm_pvid  = '".$pivcode."'  order by bnc_no asc;";
$query = mysql_query($sql,$conn);

for($i=1; $i<=mysql_num_rows($query); $i++){
	$result = mysql_fetch_array($query);
	$bnc_no = $result["bnc_no"];
	$bnc_id = $result["bnc_id"];
	$bnc_name = $result["bnc_name"];

?>
<tr align="center" height="28" onmouseover="this.style.backgroundColor = '#D4FF00';" onmouseout="this.style.backgroundColor = '';"  style="color:#007F00; cursor:pointer;"  onclick="window.open('piv_report_bill_incoming.php?dt1=<?=$dt1?>&dt2=<?=$dt2?>&bnc_id=<?=$bnc_id?>&bnc_name=<?=$bnc_name?>','winbillin','')">
	<td rowspan="4" bgcolor="#FFFFFF" style="cursor:default;"><font color="#000000"><?=$bnc_name?></font></td>
	<th align="left">&nbsp;&nbsp;บิลแทงเข้า</th>
	<th><?=$arr_money_IN[$bnc_no][0]?></th>
    <th><?=number_format($arr_money_IN[$bnc_no][1])?></th>
</tr>
<tr align="center" height="28" onmouseover="this.style.backgroundColor = '#D4FF00';" onmouseout="this.style.backgroundColor = '';"  style="color:#D40000; cursor:pointer;" onclick="window.open('piv_report_bill_payOut.php?dt1=<?=$dt1?>&dt2=<?=$dt2?>&bnc_id=<?=$bnc_id?>&bnc_name=<?=$bnc_name?>','winpayout','')">
	<td align="left">&nbsp;&nbsp;บิลจ่ายออก</td>
	<td><?=$arr_money_OUT[$bnc_no][0]?></td>
    <td><?=number_format($arr_money_OUT[$bnc_no][1])?></td>
</tr>
<tr align="center" height="28" onmouseover="this.style.backgroundColor = '#D4FF00';" onmouseout="this.style.backgroundColor = '';"  style="color:#00F; cursor:pointer;" onclick="window.open('piv_report_bill_prePay.php?dt1=<?=$dt1?>&dt2=<?=$dt2?>&bnc_id=<?=$bnc_id?>&bnc_name=<?=$bnc_name?>','winprepay','')">
	<td align="left">&nbsp;&nbsp;บิลเตรียมจ่าย</td>
	<td><?=$arr_moneyPay[$bnc_no][0]?></td>
    <td><?=number_format($arr_moneyPay[$bnc_no][1])?></td>
</tr>
<tr align="center" height="28" onmouseover="this.style.backgroundColor = '#D4FF00';" onmouseout="this.style.backgroundColor = '';"  style="color:#434343; cursor:pointer;" onclick="window.open('piv_report_bill_cancel.php?dt1=<?=$dt1?>&dt2=<?=$dt2?>&bnc_id=<?=$bnc_id?>&bnc_name=<?=$bnc_name?>','wincancel','')">
	<td align="left">&nbsp;&nbsp;บิลยกเลิก</td>
	<td><?=$arr_Bill_Cancel[$bnc_no][0]?></td>
    <td><?=number_format($arr_Bill_Cancel[$bnc_no][1])?></td>
</tr>
<tr height="6" bgcolor="#808080"><td colspan="4"></td></tr>
<?php
} // end for()
?>
</table>

<br/>
<br/>
</body>
</html>