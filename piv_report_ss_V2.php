<?phprequire_once("header-text-html.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>

<body>
<?php
require_once("condb.php");
$dt1 = $_POST["dt1"];
$dt2 = $_POST["dt2"];
$pivcode = $_POST["pivcode"];

//-----------------------ตรวจสอบสถานะการบันทึกผล การแข่งขัน ของว่าตารางว่า ครบทุกคู่หรือยัง------------------------------------------------------------//
$query_chk_table_status = mysql_query("select * from match_h where match_date between '".$dt1."' and '".$dt2."' and matchStatus !='Draft'  order by  match_date desc ;",$conn);

if(mysql_num_rows($query_chk_table_status) > 0){
	$result_chk_table_status = mysql_fetch_array($query_chk_table_status);
	if($result_chk_table_status["matchStatus"] == "Saved") $alert_table_status = '<font color="#007F00">'."การบันทึกผลสมบูรณ์แล้ว".'</font>';
	else $alert_table_status ='<font color="#D40000">'. "ยังไม่สมบูรณ์ (ตารางวันที่  " . $result_chk_table_status['match_date'] . " ยังบันทึกผลไม่ครบทุกคู่ !!.)".'</font>';

}else{	
	$alert_table_status ='<font color="#D40000">'. "ยังไม่สมบูรณ์ (ตารางวันที่  " .$dt1. " - ".$dt2."  ยังไม่บันทึกผลแข่ง !!.)".'</font>';	
}
 
//-------------------------------------------------------------------------------------##----------------------------------------------------------------///
?>

<br/>
<center>
<font size="4" color="#2A3F55"><b>สรุปรายงาน รายรับ - รายจ่าย ของแต่ละสาขา</b></font>
<br/><br/>
<font size="3"><b>สถานะการบันทึกผล :: <?=$alert_table_status?></b></font>
</center>
<br/>


<table width="95%"  align="center" border="0" cellpadding="1" cellspacing="1" bgcolor="#FFFFFF">
<tr align="center" height="40" style="color:#FFF; background-color:#2A7F55; font-size:16px;">  
        <th>ชื่อสาขา</th>
        <th width="100">พนักงาน</th>
        <th width="150">บิลรับเข้า</th>
        <th width="150">บิลเตรียมจ่าย</th>
        <th width="150">กำไร ขาดทุน</th>
        <th width="150">บิลจ่ายออก</th>
        <th width="150">บิลยกเลิก</th>
    </tr>
   
<?php
	
$sql="select * from branch where ctm_pvid  = '".$pivcode."' and bnc_status='Y'  order by bnc_no asc;";
$query = mysql_query($sql,$conn);

$net_money = 0;
$net_money_in = 0;
$net_money_prepay = 0;
$net_money_out = 0;
$net_money_cancel = 0;

$net_bill_in = 0;
$net_bill_prepay = 0;
$net_bill_out = 0;
$net_bill_cancel = 0;



for($i=1; $i<=mysql_num_rows($query); $i++){
	$result = mysql_fetch_array($query);
	$bnc_no = $result["bnc_no"];
	$bnc_id = $result["bnc_id"];
	$bnc_name = $result["bnc_name"];
//------- เงินแทงเข้า -------------------------------------//
$sql_bill_in = "SELECT branch.bnc_no, bill_h.create_by_id, count(*) as amount, sum(bill_h.costs) as sum_cost
FROM  bill_h INNER JOIN branch on bill_h.bnc_id = branch.bnc_id
where bill_h.match_date between '".$dt1."' and '".$dt2."'
and branch.ctm_pvid = '".$pivcode."'
and bill_h.cancel_flag = 'N'
and bill_h.bnc_id = '".$bnc_id."'
GROUP BY bill_h.bnc_id, bill_h.create_by_id, branch.bnc_no  
order by branch.bnc_no asc ;";
$query_bill_in = mysql_query($sql_bill_in,$conn);

$arr_bill_in = array();
for($j=1; $j<= mysql_num_rows($query_bill_in); $j++){
	$result_bill_in = mysql_fetch_array($query_bill_in);
	$arr_bill_in[strtolower($result_bill_in['create_by_id'])][0] = $result_bill_in["amount"];
	$arr_bill_in[strtolower($result_bill_in['create_by_id'])][1] = $result_bill_in["sum_cost"];
}
//-------- เงินเตรียมจ่าย -------------------------------------//
$sql_bill_prepay = "SELECT branch.bnc_no, bill_h.create_by_id, count(*) as amount, sum(bill_h.pay_amount) as pay_amount
FROM  bill_h INNER JOIN branch on bill_h.bnc_id = branch.bnc_id
where bill_h.match_date between '".$dt1."' and '".$dt2."'
and branch.ctm_pvid = '".$pivcode."'
and bill_h.pay_amount > 0
and bill_h.cancel_flag = 'N'
and bill_h.bnc_id = '".$bnc_id."'
GROUP BY bill_h.bnc_id, bill_h.create_by_id, branch.bnc_no  
order by branch.bnc_no asc ;";
$query_bill_prepay = mysql_query($sql_bill_prepay,$conn);

$arr_bill_prepay = array();
for($j=1; $j<= mysql_num_rows($query_bill_prepay); $j++){
	$result_bill_prepay = mysql_fetch_array($query_bill_prepay);
	$arr_bill_prepay[strtolower($result_bill_prepay["create_by_id"])][0] = $result_bill_prepay["amount"];
	$arr_bill_prepay[strtolower($result_bill_prepay["create_by_id"])][1] = $result_bill_prepay["pay_amount"];
}

//------------- เงินจ่ายออก -------------------------------------//
$sql_bill_out = "SELECT branch.bnc_no, bill_h.create_by_id, count(*) as amount, sum(bill_h.pay_amountAccept) as sum_pay_amount
FROM  bill_h INNER JOIN branch on bill_h.bnc_id = branch.bnc_id
where bill_h.payment_date between '".$dt1."' and '".$dt2."'
and branch.ctm_pvid = '".$pivcode."'
and bill_h.pay_amountAccept > 0
and bill_h.cancel_flag = 'N'
and bill_h.bnc_id = '".$bnc_id."'
GROUP BY bill_h.bnc_id, bill_h.create_by_id, branch.bnc_no  order by branch.bnc_no asc ;";
$query_bill_out = mysql_query($sql_bill_out,$conn);

$arr_bill_out = array();
for($j=1; $j<= mysql_num_rows($query_bill_out); $j++){
	$result_bill_out = mysql_fetch_array($query_bill_out);
	$arr_bill_out[strtolower($result_bill_out["create_by_id"])][0] = $result_bill_out["amount"];
	$arr_bill_out[strtolower($result_bill_out["create_by_id"])][1] = $result_bill_out["sum_pay_amount"];
}

//-------------- บิลยกเลิก -------------------------------------//
$sql_bill_cancel = "SELECT branch.bnc_no, bill_h.create_by_id, count(*) as amount, sum(bill_h.costs) as sum_cost_cancel
FROM  bill_h INNER JOIN branch on bill_h.bnc_id = branch.bnc_id
where bill_h.match_date between '".$dt1."' and '".$dt2."'
and branch.ctm_pvid = '".$pivcode."'
and bill_h.cancel_flag = 'Y'
and bill_h.bnc_id = '".$bnc_id."'
GROUP BY bill_h.bnc_id, bill_h.create_by_id, branch.bnc_no  order by branch.bnc_no asc ;";
$query_bill_cancel = mysql_query($sql_bill_cancel,$conn);

$arr_bill_cancel = array();
for($j=1; $j<= mysql_num_rows($query_bill_cancel); $j++){
	$result_bill_cancel = mysql_fetch_array($query_bill_cancel);
	$arr_bill_cancel[strtolower($result_bill_cancel["create_by_id"])][0] = $result_bill_cancel["amount"];
	$arr_bill_cancel[strtolower($result_bill_cancel["create_by_id"])][1] = $result_bill_cancel["sum_cost_cancel"];
}

?>
 <tr height="2" bgcolor="#0066CC"><td colspan="7"></td></tr>
 <tr align="center" bgcolor="#FFFFFF">
	<td>&nbsp;</td>
    <td colspan="6">
    
        <table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#D6D6D6">
            <tr align="center" height="19" bgcolor="#FFFFFF" style="color:#666; background-image:url(image/title_bg2.png); background-repeat:repeat-x;">
             <td width="98">ผู้ใช้งาน</td>
             <td width="58">ใบ</td>
             <td width="90">เงินแทงเข้า</td>
             <td width="58">ใบ</td>
             <td width="90">เตรียมจ่าย</td>
             <td width="148">กำไร ขาดทุน</td>
             <td width="58">ใบ</td>
             <td width="90">เงินจ่ายออก</td>
             <td width="50">ใบ</td>
             <td>เงินยกเลิก</td>
            </tr>
            
        </table>
    
    </td>
</tr>

    <tr align="center" bgcolor="#FFFFFF" height="30"  style="font-size:17px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;" >
        <td align="left">&nbsp;<font color="#000066" size="3"><?=$bnc_name?></font></td>
        <td colspan="6">

        	<table width="100%" border="0" cellpadding="1" cellspacing="1"  bgcolor="#EBEBEB">
            <?php
					$query_user = mysql_query("SELECT * FROM userinfo WHERE  bnc_id = '".$bnc_id."' ORDER BY user_no ASC;",$conn);
					for($k=1;$k<= mysql_num_rows($query_user); $k++){
						$result_user = mysql_fetch_array($query_user);
						$uid = strtolower($result_user["user_name"]);
						$profit = "";
						$profit = $arr_bill_in[$uid][1] - $arr_bill_prepay[$uid][1];
						
						$profit_color = "";
						if($profit == 0) $profit_color = "#553F55";
						else if($profit > 0) $profit_color = "#009F00";
						else if($profit < 0) $profit_color = "#FF0000";
						
						$net_money_in += $arr_bill_in[$uid][1];
						$net_money_prepay += $arr_bill_prepay[$uid][1];
						$net_money_out += $arr_bill_out[$uid][1];
						$net_money_cancel += $arr_bill_cancel[$uid][1];
						
						$net_bill_in += $arr_bill_in[$uid][0];
						$net_bill_prepay += $arr_bill_prepay[$uid][0];
						$net_bill_out += $arr_bill_out[$uid][0];
						$net_bill_cancel += $arr_bill_cancel[$uid][0];

			 ?>
                <tr align="center" height="35" bgcolor="#FFFFFF" onmousemove="this.style.backgroundColor = '#FFFF99';" onmouseout="this.style.backgroundColor = '';">	
               	 <!-- พนักงานหน้าร้าน -->
                 <td width="98" align="left" style="cursor:pointer;" onmouseover="this.style.color = '#AA3FFF';" onmouseout="this.style.color = '';" onclick="window.open('report_bill_detail.php?dt=<?=$dt2?>&bnc_id=<?=$bnc_id?>&bnc_name=<?=$bnc_name?>&user_id=<?=$uid?>','','');">&nbsp;<?=$result_user["user_name"]?></font></td>
                
                <!-- บิลแทงเข้า -->
                 <td width="58"><font color="#808080"><?=number_format($arr_bill_in[$uid][0])?></font>&nbsp;</td>
                 <td width="90" bgcolor="#AAFFFF"  onmouseover="this.style.color = '#AA3FFF';" onmouseout="this.style.color = '#00F';"   style=" color:#00F; cursor:pointer;"  onclick="window.open('piv_report_bill_incoming.php?dt1=<?=$dt1?>&dt2=<?=$dt2?>&bnc_id=<?=$bnc_id?>&bnc_name=<?=$bnc_name?>&uid=<?=$uid?>','winbillin','')"> <u><?=number_format($arr_bill_in[$uid][1])?></u>&nbsp;</td>
                 
                 <!-- บิลเตรียมจ่าย -->
                 <td width="58"><font color="#808080"><?=number_format($arr_bill_prepay[$uid][0])?></font>&nbsp;</td>
                 <td width="90" bgcolor="#FFCCFF"  onmouseover="this.style.color = '#AA3FFF';" onmouseout="this.style.color = '#D40000';"  style=" color:#D40000; cursor:pointer;" onclick="window.open('piv_report_bill_prePay.php?dt1=<?=$dt1?>&dt2=<?=$dt2?>&bnc_id=<?=$bnc_id?>&bnc_name=<?=$bnc_name?>&uid=<?=$uid?>','winprepay','')"> <u><?=number_format($arr_bill_prepay[$uid][1])?></u>&nbsp;</td>
                
                <!-- กำไรขาดทุน-->
                 <td width="148" bgcolor="#CCFFFF"><font size="5" color="<?=$profit_color?>"><?=number_format($profit);?></font></td>
                 
                 <!-- บิลจ่ายออก -->
                 <td width="58"><font color="#808080"><?=number_format($arr_bill_out[$uid][0]);?></font></td>
                 <td width="90" onmouseover="this.style.color = '#AA3FFF';" onmouseout="this.style.color = '#7F3F00';"  style=" color:#7F3F00; cursor:pointer;" onclick="window.open('piv_report_bill_payOut.php?dt1=<?=$dt1?>&dt2=<?=$dt2?>&bnc_id=<?=$bnc_id?>&bnc_name=<?=$bnc_name?>&uid=<?=$uid?>','winpayout','')"> <u><?=number_format($arr_bill_out[$uid][1]);?></u></td>
                 
                 <!-- บิลยกเลิก -->
                 <td width="50"><font color="#808080"><?=number_format($arr_bill_cancel[$uid][0]);?></font></td>
                 <td onmouseover="this.style.color = '#AA3FFF';" onmouseout="this.style.color = '#2A3F55';" style=" color:#2A3F55; cursor:pointer;" onclick="window.open('piv_report_bill_cancel.php?dt1=<?=$dt1?>&dt2=<?=$dt2?>&bnc_id=<?=$bnc_id?>&bnc_name=<?=$bnc_name?>&uid=<?=$uid?>','wincancel','')"> <u><?=number_format($arr_bill_cancel[$uid][1]);?></u></td>
                </tr>
               <?php}?>
            </table>

        </td>
    </tr>
    
    <tr height="20" bgcolor="#FFFFFF"><td colspan="7"></td></tr>

  <?php
} // end for()

$net_money = $net_money_in - $net_money_prepay;
$net_color = "";
if($net_money == 0) $net_color = "#553F55";
else if($net_money > 0) $net_color  = "#009F00";
else if($net_money < 0) $net_color  = "#FF0000";


?> 
<!-- /////////////////////////////////---------------------------/////////////////////////////////////////////////////// -->
<!-- สรุปทั้งหมด Page Footer -->
<tr align="center" height="40" bgcolor="#006633" style="color:#FFF; font-size:16px;">  
        <th colspan="2" rowspan="3"><font size="5">รายงานยอดรวม</font></th>
        <th width="150">บิลรับเข้า</th>
        <th width="150">บิลเตรียมจ่าย</th>
        <th width="150">กำไร ขาดทุน</th>
        <th width="150">บิลจ่ายออก</th>
        <th width="150">บิลยกเลิก</th>
    </tr>
    
 <tr align="center" bgcolor="#FFFFFF">
    <td colspan="5">
    
        <table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#D6D6D6">
            <tr align="center" height="19" bgcolor="#FFFFFF" style="color:#666; background-image:url(image/title_bg2.png); background-repeat:repeat-x;">
             <td width="55">ใบ</td>
             <td width="90">เงินแทงเข้า</td>
             <td width="58">ใบ</td>
             <td width="90">เตรียมจ่าย</td>
             <td width="148">กำไร ขาดทุน</td>
             <td width="58">ใบ</td>
             <td width="90">เงินจ่ายออก</td>
             <td width="50">ใบ</td>
             <td>เงินยกเลิก</td>
            </tr>
            
        </table>
    
    </td>
</tr>

    <tr align="center" bgcolor="#CCFFFF" height="30"  style="font-size:17px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;">
        <td colspan="5">
<!-- สรุปทั้งหมด Page Footer -->
        	<table width="100%" border="0" cellpadding="1" cellspacing="1"  bgcolor="#EBEBEB">

                <tr align="center" height="35" bgcolor="#FFFFFF">
                 <!-- บิลแทงเข้า -->
                 <td width="55"><font color="#808080"><?=number_format($net_bill_in)?></font>&nbsp;</td>
                 <td width="90" bgcolor="#AAFFFF"><font color="#0000FF"><?=number_format($net_money_in)?></font>&nbsp;</td>
                 
                  <!-- บิลเตรียจ่าย -->
                 <td width="58"><font color="#808080"><?=number_format($net_bill_prepay)?></font>&nbsp;</td>
                 <td width="90" bgcolor="#FFCCFF"><font color="#D40000"><?=number_format($net_money_prepay)?></font>&nbsp;</td>
                  
                  <!-- กำไรขาดทุน-->
                 <td width="148" bgcolor="#CCFFFF"><font size="5" color="<?=$net_color?>"><?=number_format($net_money);?></font></td>
                 
                 <!-- บิลจ่ายออก -->
                 <td width="58"><font color="#808080"><?=number_format($net_bill_out);?></font></td>
                 <td width="90"><font color="#7F0000"><?=number_format($net_money_out);?></font></td>
                 
                 <!-- บิลยกเลิก -->
                 <td width="50"><font color="#808080"><?=number_format($net_bill_cancel);?></font></td>
                 <td><font color="#2A3F55"><?=number_format($net_money_cancel);?></font></td>
                </tr>
       
            </table>

        </td>
    </tr>

</table>

<!-- ////////////////////////////////////-->
<br/>
<br/>

<?php
mysql_close($conn);
?>
</body>
</html>