<?php  require_once("../header-text-html.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>สถิติยอดแทงประจำวัน</title>
<style type="text/css">
<!--
body{
	font-family:Tahoma, Geneva, sans-serif;
	font-size:14px;
	background-color:#AABFFF;
	}

-->
</style>
<script type="text/javascript" src="../ajax_framework.js"></script>
<script type="text/javascript">
<!--
function view_report(){
	document.getElementById("divrptDisplay").innerHTML = "<br/><br/><center><b>กำลังสรุปรายงาน...</b><br/><img src='../image/loading.gif' /></center><br/><br/>";
	var data = "pivcode=<?=$_GET["pivcode"]?>";
	data+="&billtype=" + document.getElementById("sltBillType").value;
	data+="&match_date=" + document.getElementById("txtMatchDate").value;
	data+="&min_length=" + document.getElementById("sltMinLength").value;
	data+="&limit=" + document.getElementById("sltLimit").value;
	ajaxLoad('post','statistic_rpt_ss.php',data,'divrptDisplay');
	
}

function my_getDate(txt){
	document.getElementById("displayCalendar").style.left = event.clientX + "px";
	if(document.getElementById("displayCalendar").style.display=='block'){
		document.getElementById("displayCalendar").style.display='none';
	}
	else{
		document.getElementById("displayCalendar").style.display='block';
		window.open('../NAWEE_CALENDAR_1.php?text='+txt,'ifrmNAWEE_CALENDAR','');
	}
}
-->
</script>
</head>

<body>

<?php
require_once("../condb.php");
$sql="SELECT match_date FROM match_h WHERE matchStatus != 'Draft' ORDER BY match_date DESC LIMIT 1; ";
$query = mysql_query($sql,$conn);
$result_date = mysql_fetch_array($query);

?>

<div style="width:100%; height:60px; margin:auto; text-align:center;color:#FFF; background-color:#7F9FFF; ">
	<div style=" width:100%; height:8px;"></div>
    <table cellpadding="1" cellspacing="1" border="0" align="center"  style="border:solid 1px #F6F6F6;">
        <tr>
            <td>&nbsp;&nbsp;ประเภทบิล<select id="sltBillType"><option value="ALL">ทั้งหมด</option><option value="one">บิลเต็ง</option><option value="step">บิลสเต็ป</option></select></td>
            <td>&nbsp;&nbsp;วันที่<input type="text" id="txtMatchDate" value="<?=$result_date["match_date"];?>"  readonly="readonly" size="10" style="text-align:center; background-color:#FFF; height:18px; cursor:pointer;"  onclick="my_getDate('txtMatchDate')" />
            
     <div id="displayCalendar" style=" width:185px; height:auto; display:none; position:absolute; z-index:1000">
        <iframe name="ifrmNAWEE_CALENDAR" style="width:100%; height:175px;"  frameborder="0" marginheight="0" marginwidth="0"></iframe>
    </div>

            
            </td>
        	<td>&nbsp;&nbsp;ยอดแทงรวม ตั้งแต่<select id="sltMinLength">
            <option value="1000">1,000</option>
            <option value="3000">3,000</option>
            <option value="5000">5,000</option>
            <option value="10000" selected="selected">10,000</option>
            <option value="20000">20,000</option>
            <option value="30000">30,000</option>
            <option value="50000">50,000</option>
            <option value="100000">100,000 Up!</option> </select>&nbsp;&nbsp;</td>
            
            <td>&nbsp;&nbsp;แสดงสูงสุด<select id="sltLimit">
                <option value="3">3</option>
                <option value="5" selected="selected">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
                <option value="30">30</option>
            </select> อันดับ&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;<button type="button" onclick="view_report();" style="width:120px; height:35px; cursor:pointer;"><img src="../image/statistic24.png" /> ดูรายงาน</button>&nbsp;&nbsp;</td>
        </tr>
    </table>

    
</div>
<br/>
<div id="divrptDisplay" style="width:1100px; height:auto; margin:auto; background:#FFFFFF;"></div>

<?php
mysql_close($conn);
?>
</body>
</html>