<?php  require_once("../header-text-html.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
<br/>
<center>
<font size="4"> <u> รายงานจัดอันดับทีม ที่มีสถิติการแทงสูงสุด  <?=$_REQUEST["limit"]?> อันดับ <font color="#999999">(ไม่นับรวม สูง ต่ำ คู่ คี่)</font> </u></font>
</center>
<br/>
<table width="95%" cellpadding="2" cellspacing="1" border="0" bgcolor="#E0E0E0" align="center" style="border:solid 3px #EBEBEB;">
    <tr align="center" height="30" style=" background-color:#2A5FAA; color:#FFF;">
        <td>อันดับ</td>
        <td>เงินแทง(รวม)</td>
        <td>บิลรวม</td>
        <td>เวลาแข่ง</td>
        <td>ทีม</td>
        <td>สถานะ</td>
        <td>ชื่อลีก</td>
        <td colspan="3">ลูกต่อ/ ราคา1 /เบอร์</td>
        <td colspan="3">ลูกต่อ/ ราคา2 /เบอร์</td>
        
    </tr>
<?php
require_once("../condb.php");
$pivcode = $_REQUEST["pivcode"];
$billtype = $_POST["billtype"];
$match_date = $_REQUEST["match_date"];
$min_length = $_REQUEST["min_length"];
$limit =  $_REQUEST["limit"];

$sql= "select * from branch where ctm_pvid = '".$pivcode."' order by bnc_id asc; ";
$query = mysql_query($sql,$conn);
$num_row = mysql_num_rows($query);


for($i=1; $i<= $num_row; $i++){//for(0)
	$result = mysql_fetch_array($query);
	$bnc_id = $result["bnc_id"];		
	$bnc_name = $result["bnc_name"];		
?>

<tr height="25" bgcolor="#2A3F55" style="font-size:16px; color:#FFF;">
	<td colspan="13">&nbsp;&nbsp;&nbsp;&nbsp;<b><?=$bnc_name?></b></td>
</tr>

<?php	
$sub_sql = "SELECT COUNT(*) AS count_rec ,SUM(h.costs) AS sum_money
,i.match_detail_id ,i.bill_line_sub_stageIndex 
,m.league_name ,m.teamOfTheMatch ,m.team_A_name ,m.team_B_name ,m.match_time
,m.score_STG1 ,m.rate_STG1_A ,m.rate_STG1_B ,m.score_STG2 ,m.rate_STG2_A ,m.rate_STG2_B ,m.No_STG1A ,m.No_STG1B ,m.No_STG2A ,m.No_STG2B

FROM bill_line i INNER JOIN bill_h h ON i.bill_id = h.bill_id
INNER JOIN match_detail m ON i.match_detail_id = m.match_detail_id AND h.match_date = m.match_date
WHERE h.match_date = '".$match_date."'  AND h.cancel_flag='N' AND h.bnc_id='".$bnc_id."' AND i.bill_line_sub_stageIndex IN('A','B')
";

//ดูเฉพาะ บิลเต็ง หรือ  บิลสเต็ป
if($billtype == "one" || $billtype == "step") $sub_sql .= " AND h.bill_type = '".$billtype."' " ; 

$sub_sql .= " GROUP BY i.match_detail_id ,i.bill_line_sub_stageIndex 
HAVING sum_money >= ".$min_length."
ORDER BY sum_money  DESC  limit ".$limit." ; ";

$sub_query = mysql_query($sub_sql,$conn);


	for($j=1; $j<= mysql_num_rows($sub_query); $j++){//for(1)
		$result2 = mysql_fetch_array($sub_query);
		$team_name = $result2["team_".$result2["bill_line_sub_stageIndex"]."_name"];
		
		$home_status = "";
		if($result2["bill_line_sub_stageIndex"] == "A") $home_status = "เจ้าบ้าน";
		else if($result2["bill_line_sub_stageIndex"] == "B") $home_status = "เยือน";
		
		$status = "";
		if($result2["teamOfTheMatch"] == $result2["bill_line_sub_stageIndex"]){
			$status = "ต่อ";
			$f_color = "red";
			$sys_flag = "*";
		}
		else {
			$status = "รอง";
			$f_color = "blue";
			$sys_flag = "";
		}
?>
    <tr align="center" bgcolor="#FFFFFF" height="20"  onmousemove="this.style.backgroundColor = '#FFFF99';" onmouseout="this.style.backgroundColor = '';">
        <td width="35"><?=$j?></td>
        <td width="90" align="right" bgcolor="#FFCCFF"><b><?=number_format($result2["sum_money"])?></b>&nbsp;</td>
        <td width="50" align="center"><?=number_format($result2["count_rec"])?></td>
        <td width="60"><?=substr($result2["match_time"],0,5)?></td>
        <td width="180" align="left"><font color="<?=$f_color?>"><?=$sys_flag.$team_name?></font></td>
        <td width="80" align="left"><?=$home_status?> / <?=$status?></td>
        <td align="left">&nbsp;<font color="#2A3F55"><?=$result2["league_name"]?></font></td>
        <td width="60" bgcolor="#FFFFCC"><font color="#0000FF"><b><?=$result2["score_STG1"]?></b></font></td>
        <td width="40"><?=$result2["rate_STG1_".$result2["bill_line_sub_stageIndex"]]?></td>
        <td width="40"><font color="#FF0000">(<u><?=$result2["No_STG1".$result2["bill_line_sub_stageIndex"]]?></u>)</font></td>
        <td width="60" bgcolor="#CCFFFF"><font color="#0000FF"><b><?=$result2["score_STG2"]?></b></font></td>
        <td width="40"><?=$result2["rate_STG2_".$result2["bill_line_sub_stageIndex"]]?></td>
        <td width="40"><font color="#FF0000">(<u><?=$result2["No_STG2".$result2["bill_line_sub_stageIndex"]]?></u>)</font></td>
    </tr>
<?php
	}// edn for(1)
	
}//end for(0)


mysql_close($conn);
?>

</table>

<br/><br/>
</body>
</html>